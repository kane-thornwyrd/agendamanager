"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaginateBuyerDto = exports.BuyerDto = exports.BuyerEntity = exports.extractBuyerWithKeyTransformation = exports.extractBuyer = exports.containOnlyBuyerAttrs = exports.isABuyer = exports.buyerAttrs = void 0;
const tslib_1 = require("tslib");
const class_transformer_1 = require("class-transformer");
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const common_1 = require("./common");
exports.buyerAttrs = ['id', 'name', 'company'];
const isABuyer = (v) => !!v;
exports.isABuyer = isABuyer;
exports.containOnlyBuyerAttrs = (0, common_1.containOnlyAttrs)(exports.buyerAttrs);
const extractBuyer = (v) => (0, common_1.extractObject)(exports.buyerAttrs)(v);
exports.extractBuyer = extractBuyer;
const extractBuyerWithKeyTransformation = (keyTransformation) => (v) => (0, common_1.extractObject)((0, common_1.transformKeysArray)(keyTransformation)(exports.buyerAttrs))(v);
exports.extractBuyerWithKeyTransformation = extractBuyerWithKeyTransformation;
let BuyerEntity = class BuyerEntity {
};
tslib_1.__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('increment'),
    tslib_1.__metadata("design:type", Number)
], BuyerEntity.prototype, "id", void 0);
tslib_1.__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 255 }),
    tslib_1.__metadata("design:type", String)
], BuyerEntity.prototype, "name", void 0);
tslib_1.__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 255 }),
    tslib_1.__metadata("design:type", String)
], BuyerEntity.prototype, "company", void 0);
BuyerEntity = tslib_1.__decorate([
    (0, typeorm_1.Entity)({ name: 'buyer' })
], BuyerEntity);
exports.BuyerEntity = BuyerEntity;
class BuyerDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], BuyerDto.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], BuyerDto.prototype, "name", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], BuyerDto.prototype, "company", void 0);
exports.BuyerDto = BuyerDto;
class PaginateBuyerDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ type: [BuyerDto] }),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => BuyerDto),
    tslib_1.__metadata("design:type", Array)
], PaginateBuyerDto.prototype, "data", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], PaginateBuyerDto.prototype, "search", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], PaginateBuyerDto.prototype, "type", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], PaginateBuyerDto.prototype, "sort", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], PaginateBuyerDto.prototype, "page", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], PaginateBuyerDto.prototype, "perPage", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], PaginateBuyerDto.prototype, "total", void 0);
exports.PaginateBuyerDto = PaginateBuyerDto;
