"use strict";
/**
 * The idea is to have somewhat of a light design going on, with dumb objects limited
 * to simple data types, so we mostly kill the coupling between logic and data.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
tslib_1.__exportStar(require("./appointment.type"), exports);
tslib_1.__exportStar(require("./buyer.type"), exports);
tslib_1.__exportStar(require("./vendor.type"), exports);
tslib_1.__exportStar(require("./common"), exports);
