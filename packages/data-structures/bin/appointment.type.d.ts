import { functionWithHomogeneousType } from './common';
export type AppointmentTypeType = 'physical' | 'virtual';
export declare enum AppointmentTypeEnum {
  PHYSICAL = 'physical',
  VIRTUAL = 'virtual',
}
export type VirtualAppointmentType = {
  id: number;
  title: string;
  type: 'virtual';
  location?: string;
  link: string;
  host: string;
  client: string;
  startTime: number;
  endTime: number;
};
export type PhysicalAppointmentType = {
  id: number;
  title: string;
  type: 'physical';
  location: string;
  link?: string;
  host: string;
  client: string;
  startTime: number;
  endTime: number;
};
export type AppointmentType = PhysicalAppointmentType | VirtualAppointmentType;
export declare const appointmentAttrs: string[];
export declare const isAnAppointment: (v: any) => v is AppointmentType;
export declare const containOnlyAppointmentAttrs: import('./common').testCallback<object>;
export declare const extractAppointment: (v: object) => AppointmentType;
export declare const extractAppointmentWithKeyTransformation: (
  keyTransformation: functionWithHomogeneousType<string>
) => (v: object) => AppointmentType;
export declare class AppointmentEntity {
  id: number;
  title: string;
  type: string;
  location: string;
  link: string;
  host: string;
  client: string;
  startTime: number;
  endTime: number;
}
export declare class AppointmentDto {
  id: number;
  title: string;
  type: string;
  location?: string;
  link?: string;
  host: string;
  client: string;
  startTime: string;
  endTime: string;
}
export declare class PaginateAppointmentDto {
  data: AppointmentDto[];
  search?: string;
  type?: string;
  sort?: string;
  page: number;
  perPage: number;
  total: number;
}
