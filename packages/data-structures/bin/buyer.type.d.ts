import { functionWithHomogeneousType } from './common';
export type BuyerType = {
  id: number;
  name: string;
  company: string;
};
export declare const buyerAttrs: string[];
export declare const isABuyer: (v: any) => v is BuyerType;
export declare const containOnlyBuyerAttrs: import('./common').testCallback<object>;
export declare const extractBuyer: (v: object) => BuyerType;
export declare const extractBuyerWithKeyTransformation: (
  keyTransformation: functionWithHomogeneousType<string>
) => (v: object) => BuyerType;
export declare class BuyerEntity {
  id: number;
  name: string;
  company: string;
}
export declare class BuyerDto {
  id: number;
  name: string;
  company: string;
}
export declare class PaginateBuyerDto {
  data: BuyerDto[];
  search?: string;
  type?: string;
  sort?: string;
  page: number;
  perPage: number;
  total: number;
}
