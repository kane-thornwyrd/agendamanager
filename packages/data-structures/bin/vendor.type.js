"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaginateVendorDto = exports.VendorDto = exports.VendorEntity = exports.extractVendorWithKeyTransformation = exports.extractVendor = exports.containOnlyVendorAttrs = exports.isAVendor = exports.vendorAttrs = void 0;
const tslib_1 = require("tslib");
const class_transformer_1 = require("class-transformer");
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const common_1 = require("./common");
exports.vendorAttrs = ['id', 'name'];
const isAVendor = (v) => !!v;
exports.isAVendor = isAVendor;
exports.containOnlyVendorAttrs = (0, common_1.containOnlyAttrs)(exports.vendorAttrs);
const extractVendor = (v) => (0, common_1.extractObject)(exports.vendorAttrs)(v);
exports.extractVendor = extractVendor;
const extractVendorWithKeyTransformation = (keyTransformation) => (v) => (0, common_1.extractObject)((0, common_1.transformKeysArray)(keyTransformation)(exports.vendorAttrs))(v);
exports.extractVendorWithKeyTransformation = extractVendorWithKeyTransformation;
let VendorEntity = class VendorEntity {
};
tslib_1.__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    tslib_1.__metadata("design:type", Number)
], VendorEntity.prototype, "id", void 0);
tslib_1.__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 255 }),
    tslib_1.__metadata("design:type", String)
], VendorEntity.prototype, "name", void 0);
VendorEntity = tslib_1.__decorate([
    (0, typeorm_1.Entity)({ name: 'vendor' })
], VendorEntity);
exports.VendorEntity = VendorEntity;
class VendorDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], VendorDto.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], VendorDto.prototype, "name", void 0);
exports.VendorDto = VendorDto;
class PaginateVendorDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ type: [VendorDto] }),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => VendorDto),
    tslib_1.__metadata("design:type", Array)
], PaginateVendorDto.prototype, "data", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], PaginateVendorDto.prototype, "search", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], PaginateVendorDto.prototype, "type", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], PaginateVendorDto.prototype, "sort", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], PaginateVendorDto.prototype, "page", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], PaginateVendorDto.prototype, "perPage", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], PaginateVendorDto.prototype, "total", void 0);
exports.PaginateVendorDto = PaginateVendorDto;
