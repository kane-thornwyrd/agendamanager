export type castingFunction<T> = (v: any) => T;
export type mapCallback<T> = (v: T[]) => T[];
export type testCallback<T> = (v: T) => boolean;
export type functionWithHomogeneousType<T> = (v: T) => T;
export declare const extractObject: <T>(
  typeKeys: string[]
) => castingFunction<T>;
export declare const transformKeysArray: (
  transform: functionWithHomogeneousType<string>
) => mapCallback<string>;
export declare const containOnlyAttrs: (
  typeAttrs: any[]
) => testCallback<object>;
