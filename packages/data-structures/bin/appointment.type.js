"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaginateAppointmentDto = exports.AppointmentDto = exports.AppointmentEntity = exports.extractAppointmentWithKeyTransformation = exports.extractAppointment = exports.containOnlyAppointmentAttrs = exports.isAnAppointment = exports.appointmentAttrs = exports.AppointmentTypeEnum = void 0;
const tslib_1 = require("tslib");
const class_transformer_1 = require("class-transformer");
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const common_1 = require("./common");
var AppointmentTypeEnum;
(function (AppointmentTypeEnum) {
    AppointmentTypeEnum["PHYSICAL"] = "physical";
    AppointmentTypeEnum["VIRTUAL"] = "virtual";
})(AppointmentTypeEnum = exports.AppointmentTypeEnum || (exports.AppointmentTypeEnum = {}));
exports.appointmentAttrs = [
    'id',
    'title',
    'type',
    'location',
    'link',
    'host',
    'client',
    'startTime',
    'endTime',
];
const isAnAppointment = (v) => !!v;
exports.isAnAppointment = isAnAppointment;
exports.containOnlyAppointmentAttrs = (0, common_1.containOnlyAttrs)(exports.appointmentAttrs);
const extractAppointment = (v) => (0, common_1.extractObject)(exports.appointmentAttrs)(v);
exports.extractAppointment = extractAppointment;
const extractAppointmentWithKeyTransformation = (keyTransformation) => (v) => (0, common_1.extractObject)((0, common_1.transformKeysArray)(keyTransformation)(exports.appointmentAttrs))(v);
exports.extractAppointmentWithKeyTransformation = extractAppointmentWithKeyTransformation;
let AppointmentEntity = class AppointmentEntity {
};
tslib_1.__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    tslib_1.__metadata("design:type", Number)
], AppointmentEntity.prototype, "id", void 0);
tslib_1.__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 255 }),
    tslib_1.__metadata("design:type", String)
], AppointmentEntity.prototype, "title", void 0);
tslib_1.__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        default: AppointmentTypeEnum.PHYSICAL,
    }),
    tslib_1.__metadata("design:type", String)
], AppointmentEntity.prototype, "type", void 0);
tslib_1.__decorate([
    (0, typeorm_1.Column)({ type: 'text', nullable: true }),
    tslib_1.__metadata("design:type", String)
], AppointmentEntity.prototype, "location", void 0);
tslib_1.__decorate([
    (0, typeorm_1.Column)({ type: 'text', nullable: true }),
    tslib_1.__metadata("design:type", String)
], AppointmentEntity.prototype, "link", void 0);
tslib_1.__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 255 }),
    tslib_1.__metadata("design:type", String)
], AppointmentEntity.prototype, "host", void 0);
tslib_1.__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 255 }),
    tslib_1.__metadata("design:type", String)
], AppointmentEntity.prototype, "client", void 0);
tslib_1.__decorate([
    (0, typeorm_1.Column)({ type: 'datetime' }),
    tslib_1.__metadata("design:type", Number)
], AppointmentEntity.prototype, "startTime", void 0);
tslib_1.__decorate([
    (0, typeorm_1.Column)({ type: 'datetime' }),
    tslib_1.__metadata("design:type", Number)
], AppointmentEntity.prototype, "endTime", void 0);
AppointmentEntity = tslib_1.__decorate([
    (0, typeorm_1.Entity)({ name: 'appointment' })
], AppointmentEntity);
exports.AppointmentEntity = AppointmentEntity;
class AppointmentDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], AppointmentDto.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({
        default: 'new Appointment'
    }),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], AppointmentDto.prototype, "title", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({
        enum: AppointmentTypeEnum,
        default: AppointmentTypeEnum.VIRTUAL
    }),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], AppointmentDto.prototype, "type", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({
        nullable: true
    }),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], AppointmentDto.prototype, "location", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({
        nullable: true
    }),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], AppointmentDto.prototype, "link", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], AppointmentDto.prototype, "host", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], AppointmentDto.prototype, "client", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({
        format: 'date-time'
    }),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], AppointmentDto.prototype, "startTime", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({
        format: 'date-time'
    }),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], AppointmentDto.prototype, "endTime", void 0);
exports.AppointmentDto = AppointmentDto;
class PaginateAppointmentDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ type: [AppointmentDto] }),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => AppointmentDto),
    tslib_1.__metadata("design:type", Array)
], PaginateAppointmentDto.prototype, "data", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], PaginateAppointmentDto.prototype, "search", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], PaginateAppointmentDto.prototype, "type", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    tslib_1.__metadata("design:type", String)
], PaginateAppointmentDto.prototype, "sort", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], PaginateAppointmentDto.prototype, "page", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], PaginateAppointmentDto.prototype, "perPage", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, class_transformer_1.Type)(() => Number),
    tslib_1.__metadata("design:type", Number)
], PaginateAppointmentDto.prototype, "total", void 0);
exports.PaginateAppointmentDto = PaginateAppointmentDto;
