import { functionWithHomogeneousType } from './common';
export type VendorType = {
  id: number;
  name: string;
};
export declare const vendorAttrs: string[];
export declare const isAVendor: (v: any) => v is VendorType;
export declare const containOnlyVendorAttrs: import('./common').testCallback<object>;
export declare const extractVendor: (v: object) => VendorType;
export declare const extractVendorWithKeyTransformation: (
  keyTransformation: functionWithHomogeneousType<string>
) => (v: object) => VendorType;
export declare class VendorEntity {
  id: number;
  name: string;
}
export declare class VendorDto {
  id: number;
  name: string;
}
export declare class PaginateVendorDto {
  data: VendorDto[];
  search?: string;
  type?: string;
  sort?: string;
  page: number;
  perPage: number;
  total: number;
}
