import { describe, expect, it } from '@jest/globals';

import {
  VendorType,
  isAVendor,
  extractVendor,
  extractVendorWithKeyTransformation,
  containOnlyVendorAttrs,
} from '../src/index';

import {
  BuyerType,
  isABuyer,
  extractBuyer,
  extractBuyerWithKeyTransformation,
  containOnlyBuyerAttrs,
} from '../src/index';

import {
  AppointmentType,
  isAnAppointment,
  extractAppointment,
  extractAppointmentWithKeyTransformation,
  containOnlyAppointmentAttrs,
} from '../src/index';

describe('The `Vendor` interface', () => {
  const validVendor: VendorType = {
    id: 0,
    name: 'foo',
  };

  const invalidVendor = {
    id: 'bad id',
    name: 'foo',
  };

  const nonSpecificObjectThatContainAVendor = {
    id: 0,
    name: 'foo',
    foo: false,
    bar: 10,
  };

  const nonSpecificObjectThatContainAVendorPrefixed = {
    vendorId: 0,
    vendorName: 'foo',
    id: 6,
    name: 'bar',
    foo: false,
    bar: 10,
  };

  const nonSpecificObjectThatDoesNotContainAVendor = {
    id: 0,
    foo: false,
    bar: 10,
  };

  it('has a function to tell if something could be a valid one', () =>
    Promise.all([
      expect(isAVendor(validVendor)),
      expect(isAVendor(nonSpecificObjectThatContainAVendor)),
      expect(!isAVendor(invalidVendor)),
      expect(!containOnlyVendorAttrs(nonSpecificObjectThatContainAVendor)),
      expect(
        !containOnlyVendorAttrs(nonSpecificObjectThatDoesNotContainAVendor)
      ),
    ]));
  it('has a function to extract one from a given non-specific Object', () =>
    Promise.all([
      expect(isAVendor(extractVendor(nonSpecificObjectThatContainAVendor))),
      expect(
        !isAVendor(extractVendor(nonSpecificObjectThatDoesNotContainAVendor))
      ),
    ]));
  it(
    'has a function to extract one from a given non-specific Object, \n\t' +
      'using a callback to change the keys on-the-fly',
    () => {
      const result = extractVendorWithKeyTransformation((v) => `vendor${v}`)(
        nonSpecificObjectThatContainAVendorPrefixed
      );
      return Promise.all([
        expect(isAVendor(extractVendor(result))),
        expect(result.id === 0),
        expect(result.name === 'foo'),
      ]);
    }
  );
});

describe('The `Buyer` interface', () => {
  const validBuyer: BuyerType = {
    id: 0,
    name: 'foo',
    company: 'bar',
  };

  const invalidBuyer = {
    id: 'bad id',
    name: 'foo',
    company: 'bar',
  };

  const nonSpecificObjectThatContainABuyer = {
    id: 0,
    name: 'foo',
    company: 'bar',
    foo: false,
    bar: 10,
  };

  const nonSpecificObjectThatContainABuyerPrefixed = {
    buyerId: 0,
    buyerName: 'foo',
    buyerCompany: 'bar',
    id: 6,
    name: 'bar',
    foo: false,
    bar: 10,
  };

  const nonSpecificObjectThatDoesNotContainABuyer = {
    id: 0,
    foo: false,
    bar: 10,
  };

  it('has a function to tell if something could be a valid one', () =>
    Promise.all([
      expect(isABuyer(validBuyer)),
      expect(isABuyer(nonSpecificObjectThatContainABuyer)),
      expect(!isABuyer(invalidBuyer)),
      expect(!containOnlyBuyerAttrs(nonSpecificObjectThatContainABuyer)),
      expect(!containOnlyBuyerAttrs(nonSpecificObjectThatDoesNotContainABuyer)),
    ]));
  it('has a function to extract one from a given non-specific Object', () =>
    Promise.all([
      expect(isABuyer(extractBuyer(nonSpecificObjectThatContainABuyer))),
      expect(
        !isABuyer(extractBuyer(nonSpecificObjectThatDoesNotContainABuyer))
      ),
    ]));
  it(
    'has a function to extract one from a given non-specific Object, \n\t' +
      'using a callback to change the keys on-the-fly',
    () => {
      const result = extractBuyerWithKeyTransformation((v) => `Buyer${v}`)(
        nonSpecificObjectThatContainABuyerPrefixed
      );
      return Promise.all([
        expect(isABuyer(extractBuyer(result))),
        expect(result.id === 0),
        expect(result.name === 'foo'),
        expect(result.company === 'bar'),
      ]);
    }
  );
});

describe('The `Appointment` interface', () => {
  const validAppointment: AppointmentType = {
    id: 0,
    title: 'foo',
    type: 'virtual',
    link: 'link value',
    host: 'host name',
    client: 'client name',
    startTime: 0,
    endTime: 3600,
  };

  const invalidAppointment = {
    id: 'bad id',
    name: 'foo',
    company: 'bar',
  };

  const nonSpecificObjectThatContainAnAppointment = {
    id: 0,
    title: 'foo',
    type: 'virtual',
    link: 'link value',
    host: 'host name',
    client: 'client name',
    startTime: 0,
    endTime: 3600,
    name: 'foo',
    company: 'bar',
    foo: false,
    bar: 10,
  };

  const nonSpecificObjectThatContainAnAppointmentPrefixed = {
    appointmentId: 0,
    appointmentTitle: 'foo',
    appointmentType: 'virtual',
    appointmentLink: 'link value',
    appointmentHost: 'host name',
    appointmentClient: 'client name',
    appointmentStartTime: 0,
    appointmentEndTime: 3600,
    id: 6,
    name: 'bar',
    foo: false,
    bar: 10,
  };

  const nonSpecificObjectThatDoesNotContainAnAppointment = {
    id: 0,
    foo: false,
    bar: 10,
  };

  it('has a function to tell if something could be a valid one', () =>
    Promise.all([
      expect(isAnAppointment(validAppointment)),
      expect(isAnAppointment(nonSpecificObjectThatContainAnAppointment)),
      expect(!isAnAppointment(invalidAppointment)),
      expect(
        !containOnlyAppointmentAttrs(nonSpecificObjectThatContainAnAppointment)
      ),
      expect(
        !containOnlyAppointmentAttrs(
          nonSpecificObjectThatDoesNotContainAnAppointment
        )
      ),
    ]));
  it('has a function to extract one from a given non-specific Object', () =>
    Promise.all([
      expect(
        isAnAppointment(
          extractAppointment(nonSpecificObjectThatContainAnAppointment)
        )
      ),
      expect(
        !isAnAppointment(
          extractAppointment(nonSpecificObjectThatDoesNotContainAnAppointment)
        )
      ),
    ]));
  it(
    'has a function to extract one from a given non-specific Object, \n\t' +
      'using a callback to change the keys on-the-fly',
    () => {
      const result = extractAppointmentWithKeyTransformation(
        (v) => `appointment${v}`
      )(nonSpecificObjectThatContainAnAppointmentPrefixed);
      return Promise.all([
        expect(isAnAppointment(extractAppointment(result))),
        expect(result.id === 0),
        expect(result.title === 'foo'),
        expect(result.type === 'virtual'),
        expect(result.link === 'link value'),
        expect(result.host === 'host name'),
        expect(result.client === 'client name'),
        expect(result.startTime === 0),
        expect(result.endTime === 3600),
      ]);
    }
  );
});
