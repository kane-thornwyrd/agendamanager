/**
 * The idea is to have somewhat of a light design going on, with dumb objects limited
 * to simple data types, so we mostly kill the coupling between logic and data.
 */

export * from './appointment.type';
export * from './buyer.type';
export * from './vendor.type';
export * from './common';
