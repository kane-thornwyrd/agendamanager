import { Expose, Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import {
  containOnlyAttrs,
  extractObject,
  functionWithHomogeneousType,
  transformKeysArray,
} from './common';

export type BuyerType = {
  id: number;
  name: string;
  company: string;
};

export const buyerAttrs = ['id', 'name', 'company'];

export const isABuyer = (v: any): v is BuyerType => !!(v as BuyerType);

export const containOnlyBuyerAttrs = containOnlyAttrs(buyerAttrs);

export const extractBuyer = (v: object): BuyerType =>
  extractObject<BuyerType>(buyerAttrs)(v);

export const extractBuyerWithKeyTransformation =
  (keyTransformation: functionWithHomogeneousType<string>) =>
  (v: object): BuyerType =>
    extractObject<BuyerType>(transformKeysArray(keyTransformation)(buyerAttrs))(
      v
    );

@Entity({ name: 'buyer' })
export class BuyerEntity {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'varchar', length: 255 })
  name: string;

  @Column({ type: 'varchar', length: 255 })
  company: string;
}

export class BuyerDto {
  @ApiProperty()
  @Expose()
  @Type(() => Number)
  id: number;

  @ApiProperty()
  @Expose()
  name: string;

  @ApiProperty()
  @Expose()
  company: string;
}

export class PaginateBuyerDto {
  @ApiProperty({ type: [BuyerDto] })
  @Expose()
  @Type(() => BuyerDto)
  data: BuyerDto[];

  @ApiProperty()
  @Expose()
  search?: string;

  @ApiProperty()
  @Expose()
  type?: string;

  @ApiProperty()
  @Expose()
  sort?: string;

  @ApiProperty()
  @Expose()
  @Type(() => Number)
  page: number;

  @ApiProperty()
  @Expose()
  @Type(() => Number)
  perPage: number;

  @ApiProperty()
  @Expose()
  @Type(() => Number)
  total: number;
}
