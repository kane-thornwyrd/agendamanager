import { Expose, Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import {
  containOnlyAttrs,
  extractObject,
  functionWithHomogeneousType,
  transformKeysArray,
} from './common';

export type AppointmentTypeType = 'physical' | 'virtual';
export enum AppointmentTypeEnum {
  PHYSICAL = 'physical',
  VIRTUAL = 'virtual',
}

export type VirtualAppointmentType = {
  id: number;
  title: string;
  type: 'virtual';
  location?: string;
  link: string;
  host: string; // Vendor
  client: string; // Appointment
  startTime: number;
  endTime: number;
};

export type PhysicalAppointmentType = {
  id: number;
  title: string;
  type: 'physical';
  location: string;
  link?: string;
  host: string; // Vendor
  client: string; // Appointment
  startTime: number;
  endTime: number;
};

export type AppointmentType = PhysicalAppointmentType | VirtualAppointmentType;

export const appointmentAttrs = [
  'id',
  'title',
  'type',
  'location',
  'link',
  'host',
  'client',
  'startTime',
  'endTime',
];

export const isAnAppointment = (v: any): v is AppointmentType =>
  !!(v as AppointmentType);

export const containOnlyAppointmentAttrs = containOnlyAttrs(appointmentAttrs);

export const extractAppointment = (v: object): AppointmentType =>
  extractObject<AppointmentType>(appointmentAttrs)(v);

export const extractAppointmentWithKeyTransformation =
  (keyTransformation: functionWithHomogeneousType<string>) =>
  (v: object): AppointmentType =>
    extractObject<AppointmentType>(
      transformKeysArray(keyTransformation)(appointmentAttrs)
    )(v);

@Entity({ name: 'appointment' })
export class AppointmentEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  title: string;

  @Column({
    type: 'varchar',
    default: AppointmentTypeEnum.PHYSICAL,
  })
  type: string;

  @Column({ type: 'text', nullable: true })
  location: string; // Ie.: Musée du Louvre rue de Rivoli, 75001 Paris, France → ChIJD3uTd9hx5kcR1IQvGfr8dbk

  @Column({ type: 'text', nullable: true })
  link: string;

  @Column({ type: 'varchar', length: 255 })
  host: string;

  @Column({ type: 'varchar', length: 255 })
  client: string;

  @Column({ type: 'datetime' })
  startTime: number;

  @Column({ type: 'datetime' })
  endTime: number;
}

export class AppointmentDto {
  @ApiProperty()
  @Expose()
  @Type(() => Number)
  id: number;

  @ApiProperty({
    default: 'new Appointment',
  })
  @Expose()
  title: string;

  @ApiProperty({
    enum: AppointmentTypeEnum,
    default: AppointmentTypeEnum.VIRTUAL,
  })
  @Expose()
  type: string;

  @ApiProperty({
    nullable: true,
  })
  @Expose()
  location?: string;

  @ApiProperty({
    nullable: true,
  })
  @Expose()
  link?: string;

  @ApiProperty()
  @Expose()
  host: string;

  @ApiProperty()
  @Expose()
  client: string;

  @ApiProperty({
    format: 'date-time',
  })
  @Expose()
  startTime: string;

  @ApiProperty({
    format: 'date-time',
  })
  @Expose()
  endTime: string;
}

export class PaginateAppointmentDto {
  @ApiProperty({ type: [AppointmentDto] })
  @Expose()
  @Type(() => AppointmentDto)
  data: AppointmentDto[];

  @ApiProperty()
  @Expose()
  search?: string;

  @ApiProperty()
  @Expose()
  type?: string;

  @ApiProperty()
  @Expose()
  sort?: string;

  @ApiProperty()
  @Expose()
  @Type(() => Number)
  page: number;

  @ApiProperty()
  @Expose()
  @Type(() => Number)
  perPage: number;

  @ApiProperty()
  @Expose()
  @Type(() => Number)
  total: number;
}
