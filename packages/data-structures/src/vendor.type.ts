import { Expose, Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import {
  containOnlyAttrs,
  extractObject,
  functionWithHomogeneousType,
  transformKeysArray,
} from './common';

export type VendorType = {
  id: number;
  name: string;
};

export const vendorAttrs = ['id', 'name'];

export const isAVendor = (v: any): v is VendorType => !!(v as VendorType);

export const containOnlyVendorAttrs = containOnlyAttrs(vendorAttrs);

export const extractVendor = (v: object): VendorType =>
  extractObject<VendorType>(vendorAttrs)(v);

export const extractVendorWithKeyTransformation =
  (keyTransformation: functionWithHomogeneousType<string>) =>
  (v: object): VendorType =>
    extractObject<VendorType>(
      transformKeysArray(keyTransformation)(vendorAttrs)
    )(v);

@Entity({ name: 'vendor' })
export class VendorEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  name: string;
}

export class VendorDto {
  @ApiProperty()
  @Expose()
  @Type(() => Number)
  id: number;

  @ApiProperty()
  @Expose()
  name: string;
}

export class PaginateVendorDto {
  @ApiProperty({ type: [VendorDto] })
  @Expose()
  @Type(() => VendorDto)
  data: VendorDto[];

  @ApiProperty()
  @Expose()
  search?: string;

  @ApiProperty()
  @Expose()
  type?: string;

  @ApiProperty()
  @Expose()
  sort?: string;

  @ApiProperty()
  @Expose()
  @Type(() => Number)
  page: number;

  @ApiProperty()
  @Expose()
  @Type(() => Number)
  perPage: number;

  @ApiProperty()
  @Expose()
  @Type(() => Number)
  total: number;
}
