import { pick, difference } from 'lodash/fp';

export type castingFunction<T> = (v: any) => T;
export type mapCallback<T> = (v: T[]) => T[];
export type testCallback<T> = (v: T) => boolean;
export type functionWithHomogeneousType<T> = (v: T) => T;

export const extractObject = <T>(typeKeys: string[]): castingFunction<T> =>
  pick(typeKeys);

export const transformKeysArray =
  (transform: functionWithHomogeneousType<string>): mapCallback<string> =>
  (typeKeys: string[]) =>
    typeKeys.map(transform);

export const containOnlyAttrs =
  (typeAttrs: any[]): testCallback<object> =>
  (v: object): boolean =>
    difference(typeAttrs, Object.keys(v)).length === 0;
