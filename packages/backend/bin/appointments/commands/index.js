"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
tslib_1.__exportStar(require("./appointment-create.command"), exports);
tslib_1.__exportStar(require("./appointment-update.command"), exports);
tslib_1.__exportStar(require("./appointment-destroy.command"), exports);
tslib_1.__exportStar(require("./handlers"), exports);
