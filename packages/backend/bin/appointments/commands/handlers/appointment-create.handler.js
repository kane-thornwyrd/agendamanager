"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentCreateHandler = void 0;
const tslib_1 = require("tslib");
const cqrs_1 = require("@nestjs/cqrs");
const data_structures_1 = require("@agendamanager/data-structures");
const class_transformer_1 = require("class-transformer");
const common_1 = require("@nestjs/common");
const appointments_repository_1 = require("../../appointments.repository");
const appointment_create_command_1 = require("../appointment-create.command");
// TODO REFACTOR USING FACTORY AND EventPublisher !!!!
let AppointmentCreateHandler = class AppointmentCreateHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ AppointmentCreate: appointment, }) {
        try {
            const newAppointment = this.repository.create();
            Object.keys(appointment).forEach((key) => {
                const value = appointment[key];
                newAppointment[key] = value;
            });
            await this.repository.save(newAppointment);
            return (0, class_transformer_1.plainToClass)(data_structures_1.AppointmentDto, newAppointment, {
                excludeExtraneousValues: true,
            });
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
AppointmentCreateHandler = tslib_1.__decorate([
    (0, cqrs_1.CommandHandler)(appointment_create_command_1.AppointmentCreateCommand),
    tslib_1.__metadata("design:paramtypes", [appointments_repository_1.AppointmentsRepository])
], AppointmentCreateHandler);
exports.AppointmentCreateHandler = AppointmentCreateHandler;
