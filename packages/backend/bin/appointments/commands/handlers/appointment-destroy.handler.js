"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentDestroyHandler = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const appointments_repository_1 = require("../../appointments.repository");
const appointment_destroy_command_1 = require("../appointment-destroy.command");
let AppointmentDestroyHandler = class AppointmentDestroyHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ id }) {
        try {
            const appointment = await this.repository.findOneBy({ id });
            await this.repository.remove(appointment);
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
AppointmentDestroyHandler = tslib_1.__decorate([
    (0, cqrs_1.CommandHandler)(appointment_destroy_command_1.AppointmentDestroyCommand),
    tslib_1.__metadata("design:paramtypes", [appointments_repository_1.AppointmentsRepository])
], AppointmentDestroyHandler);
exports.AppointmentDestroyHandler = AppointmentDestroyHandler;
