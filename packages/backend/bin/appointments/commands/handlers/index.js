"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandHandlers = void 0;
const appointment_create_handler_1 = require("./appointment-create.handler");
const appointment_update_handler_1 = require("./appointment-update.handler");
const appointment_destroy_handler_1 = require("./appointment-destroy.handler");
exports.CommandHandlers = [
    appointment_create_handler_1.AppointmentCreateHandler,
    appointment_update_handler_1.AppointmentUpdateHandler,
    appointment_destroy_handler_1.AppointmentDestroyHandler,
];
