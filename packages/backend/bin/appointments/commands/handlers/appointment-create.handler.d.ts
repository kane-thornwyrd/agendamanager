import { ICommandHandler } from '@nestjs/cqrs';
import { AppointmentDto } from '@agendamanager/data-structures';
import { AppointmentsRepository } from '../../appointments.repository';
import { AppointmentCreateCommand } from '../appointment-create.command';
export declare class AppointmentCreateHandler
  implements ICommandHandler<AppointmentCreateCommand>
{
  private repository;
  constructor(repository: AppointmentsRepository);
  execute({
    AppointmentCreate: appointment,
  }: AppointmentCreateCommand): Promise<AppointmentDto>;
}
