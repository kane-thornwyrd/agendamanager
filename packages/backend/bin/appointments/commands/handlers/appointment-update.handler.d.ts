import { ICommandHandler } from '@nestjs/cqrs';
import { AppointmentDto } from '@agendamanager/data-structures';
import { AppointmentsRepository } from '../../appointments.repository';
import { AppointmentUpdateCommand } from '../appointment-update.command';
export declare class AppointmentUpdateHandler
  implements ICommandHandler<AppointmentUpdateCommand>
{
  private repository;
  constructor(repository: AppointmentsRepository);
  execute({
    id,
    appointmentUpdate,
  }: AppointmentUpdateCommand): Promise<AppointmentDto>;
}
