import { ICommandHandler } from '@nestjs/cqrs';
import { AppointmentsRepository } from '../../appointments.repository';
import { AppointmentDestroyCommand } from '../appointment-destroy.command';
export declare class AppointmentDestroyHandler
  implements ICommandHandler<AppointmentDestroyCommand>
{
  private repository;
  constructor(repository: AppointmentsRepository);
  execute({ id }: AppointmentDestroyCommand): Promise<void>;
}
