import { AppointmentCreateHandler } from './appointment-create.handler';
import { AppointmentUpdateHandler } from './appointment-update.handler';
import { AppointmentDestroyHandler } from './appointment-destroy.handler';
export declare const CommandHandlers: (
  | typeof AppointmentCreateHandler
  | typeof AppointmentUpdateHandler
  | typeof AppointmentDestroyHandler
)[];
