"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentUpdateHandler = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const class_transformer_1 = require("class-transformer");
const data_structures_1 = require("@agendamanager/data-structures");
const appointments_repository_1 = require("../../appointments.repository");
const appointment_update_command_1 = require("../appointment-update.command");
let AppointmentUpdateHandler = class AppointmentUpdateHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ id, appointmentUpdate, }) {
        try {
            const appointment = await this.repository.findOneBy({ id });
            Object.keys(appointmentUpdate).forEach((key) => {
                const value = appointmentUpdate[key];
                appointment[key] = value;
            });
            await this.repository.save(appointment);
            return (0, class_transformer_1.plainToClass)(data_structures_1.AppointmentDto, appointment, {
                excludeExtraneousValues: true,
            });
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
AppointmentUpdateHandler = tslib_1.__decorate([
    (0, cqrs_1.CommandHandler)(appointment_update_command_1.AppointmentUpdateCommand),
    tslib_1.__metadata("design:paramtypes", [appointments_repository_1.AppointmentsRepository])
], AppointmentUpdateHandler);
exports.AppointmentUpdateHandler = AppointmentUpdateHandler;
