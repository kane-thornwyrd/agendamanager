import { AppointmentCreateRequest } from '../requests';
export declare class AppointmentCreateCommand {
  readonly AppointmentCreate: AppointmentCreateRequest;
  constructor(AppointmentCreate: AppointmentCreateRequest);
}
