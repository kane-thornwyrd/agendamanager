"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentDestroyCommand = void 0;
class AppointmentDestroyCommand {
    constructor(id) {
        this.id = id;
    }
}
exports.AppointmentDestroyCommand = AppointmentDestroyCommand;
