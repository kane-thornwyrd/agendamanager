import { AppointmentUpdateRequest } from '../requests';
export declare class AppointmentUpdateCommand {
  readonly id: number;
  readonly appointmentUpdate: AppointmentUpdateRequest;
  constructor(id: number, appointmentUpdate: AppointmentUpdateRequest);
}
