"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentUpdateCommand = void 0;
class AppointmentUpdateCommand {
    constructor(id, appointmentUpdate) {
        this.id = id;
        this.appointmentUpdate = appointmentUpdate;
    }
}
exports.AppointmentUpdateCommand = AppointmentUpdateCommand;
