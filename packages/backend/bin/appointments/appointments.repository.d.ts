import { AppointmentEntity } from '@agendamanager/data-structures';
import { BaseRepository } from '../infrastructure/db/typeorm/base.repository';
export declare class AppointmentsRepository extends BaseRepository<AppointmentEntity> {}
