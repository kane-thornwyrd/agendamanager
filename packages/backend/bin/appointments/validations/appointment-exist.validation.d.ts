import {
  ValidationOptions,
  ValidatorConstraintInterface,
} from 'class-validator';
import { AppointmentsRepository } from '../appointments.repository';
export declare class AppointmentExistsValidation
  implements ValidatorConstraintInterface
{
  private repository;
  constructor(repository: AppointmentsRepository);
  validate(id: string): Promise<boolean>;
  defaultMessage(): string;
}
/**
 *
 * @param {object} validationOptions not much in this case
 * @returns {function} a factory for a custom validator decorator
 */
export declare function AppointmentExists(
  validationOptions?: ValidationOptions
): (object: any, propertyName: string) => void;
