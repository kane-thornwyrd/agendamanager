"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentExists = exports.AppointmentExistsValidation = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const appointments_repository_1 = require("../appointments.repository");
let AppointmentExistsValidation = class AppointmentExistsValidation {
    constructor(repository) {
        this.repository = repository;
    }
    async validate(id) {
        try {
            const appointment = await this.repository.findOneBy({ id: +id });
            return !!appointment;
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
    defaultMessage() {
        return `Appointment does not exists.`;
    }
};
AppointmentExistsValidation = tslib_1.__decorate([
    (0, class_validator_1.ValidatorConstraint)({ name: 'appointmentExists', async: true }),
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [appointments_repository_1.AppointmentsRepository])
], AppointmentExistsValidation);
exports.AppointmentExistsValidation = AppointmentExistsValidation;
/**
 *
 * @param {object} validationOptions not much in this case
 * @returns {function} a factory for a custom validator decorator
 */
function AppointmentExists(validationOptions) {
    return function (object, propertyName) {
        (0, class_validator_1.registerDecorator)({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            validator: AppointmentExistsValidation,
        });
    };
}
exports.AppointmentExists = AppointmentExists;
