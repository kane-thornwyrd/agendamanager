"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentShowQuery = void 0;
class AppointmentShowQuery {
    constructor(id) {
        this.id = id;
    }
}
exports.AppointmentShowQuery = AppointmentShowQuery;
