"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentPaginateQuery = void 0;
class AppointmentPaginateQuery {
    constructor(appointmentPaginate) {
        this.appointmentPaginate = appointmentPaginate;
    }
}
exports.AppointmentPaginateQuery = AppointmentPaginateQuery;
