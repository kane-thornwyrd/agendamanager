"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryHandlers = void 0;
const appointments_handler_1 = require("./appointments.handler");
exports.QueryHandlers = [
    appointments_handler_1.AppointmentPaginateHandler,
    appointments_handler_1.AppointmentShowHandler,
];
