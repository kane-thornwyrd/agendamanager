import {
  AppointmentPaginateHandler,
  AppointmentShowHandler,
} from './appointments.handler';
export declare const QueryHandlers: (
  | typeof AppointmentPaginateHandler
  | typeof AppointmentShowHandler
)[];
