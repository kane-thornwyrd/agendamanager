"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentShowHandler = exports.AppointmentPaginateHandler = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const class_transformer_1 = require("class-transformer");
const data_structures_1 = require("@agendamanager/data-structures");
const appointments_repository_1 = require("../../appointments.repository");
const appointment_paginate_query_1 = require("../appointment-paginate.query");
const appointment_show_query_1 = require("../appointment-show.query");
let AppointmentPaginateHandler = class AppointmentPaginateHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ appointmentPaginate, }) {
        const result = await this.repository.paginate(appointmentPaginate);
        return (0, class_transformer_1.plainToClass)(data_structures_1.PaginateAppointmentDto, result, {
            excludeExtraneousValues: true,
        });
    }
};
AppointmentPaginateHandler = tslib_1.__decorate([
    (0, cqrs_1.QueryHandler)(appointment_paginate_query_1.AppointmentPaginateQuery),
    tslib_1.__metadata("design:paramtypes", [appointments_repository_1.AppointmentsRepository])
], AppointmentPaginateHandler);
exports.AppointmentPaginateHandler = AppointmentPaginateHandler;
let AppointmentShowHandler = class AppointmentShowHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ id }) {
        try {
            const appointment = await this.repository.findOneBy({ id });
            return (0, class_transformer_1.plainToClass)(data_structures_1.AppointmentDto, appointment, {
                excludeExtraneousValues: true,
            });
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
AppointmentShowHandler = tslib_1.__decorate([
    (0, cqrs_1.QueryHandler)(appointment_show_query_1.AppointmentShowQuery),
    tslib_1.__metadata("design:paramtypes", [appointments_repository_1.AppointmentsRepository])
], AppointmentShowHandler);
exports.AppointmentShowHandler = AppointmentShowHandler;
