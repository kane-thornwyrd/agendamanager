import { IQueryHandler } from '@nestjs/cqrs';
import {
  PaginateAppointmentDto,
  AppointmentDto,
} from '@agendamanager/data-structures';
import { AppointmentsRepository } from '../../appointments.repository';
import { AppointmentPaginateQuery } from '../appointment-paginate.query';
import { AppointmentShowQuery } from '../appointment-show.query';
export declare class AppointmentPaginateHandler
  implements IQueryHandler<AppointmentPaginateQuery>
{
  private repository;
  constructor(repository: AppointmentsRepository);
  execute({
    appointmentPaginate,
  }: AppointmentPaginateQuery): Promise<PaginateAppointmentDto>;
}
export declare class AppointmentShowHandler
  implements IQueryHandler<AppointmentShowQuery>
{
  private repository;
  constructor(repository: AppointmentsRepository);
  execute({ id }: AppointmentShowQuery): Promise<AppointmentDto>;
}
