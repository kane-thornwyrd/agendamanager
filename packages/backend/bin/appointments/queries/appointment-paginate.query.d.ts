import { AppointmentPaginateRequest } from '../requests';
export declare class AppointmentPaginateQuery {
  readonly appointmentPaginate: AppointmentPaginateRequest;
  constructor(appointmentPaginate: AppointmentPaginateRequest);
}
