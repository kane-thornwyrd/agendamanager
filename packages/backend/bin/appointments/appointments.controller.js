"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentsController = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const swagger_1 = require("@nestjs/swagger");
const data_structures_1 = require("@agendamanager/data-structures");
const commands_1 = require("./commands");
const queries_1 = require("./queries");
const requests_1 = require("./requests");
let AppointmentsController = class AppointmentsController {
    constructor(commandBus, queryBus) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
    }
    async index(AppointmentPaginate) {
        return this.queryBus.execute(new queries_1.AppointmentPaginateQuery(AppointmentPaginate));
    }
    async store(AppointmentCreate) {
        return this.commandBus.execute(new commands_1.AppointmentCreateCommand(AppointmentCreate));
    }
    async show({ id }) {
        return this.queryBus.execute(new queries_1.AppointmentShowQuery(+id));
    }
    async update({ id }, AppointmentUpdate) {
        return this.commandBus.execute(new commands_1.AppointmentUpdateCommand(id, AppointmentUpdate));
    }
    async destroy({ id }) {
        await this.commandBus.execute(new commands_1.AppointmentDestroyCommand(id));
    }
};
tslib_1.__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiOperation)({ summary: 'appointments Paginated' }),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: data_structures_1.PaginateAppointmentDto,
    }),
    tslib_1.__param(0, (0, common_1.Query)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.AppointmentPaginateRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], AppointmentsController.prototype, "index", null);
tslib_1.__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create a new Appointment' }),
    (0, swagger_1.ApiResponse)({
        status: 201,
        type: data_structures_1.AppointmentDto,
    }),
    (0, common_1.HttpCode)(201),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.AppointmentCreateRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], AppointmentsController.prototype, "store", null);
tslib_1.__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiOperation)({ summary: 'Show a Appointment' }),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: data_structures_1.AppointmentDto,
    }),
    (0, common_1.HttpCode)(200),
    tslib_1.__param(0, (0, common_1.Param)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.AppointmentParamRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], AppointmentsController.prototype, "show", null);
tslib_1.__decorate([
    (0, common_1.Put)('/:id'),
    (0, swagger_1.ApiOperation)({ summary: 'Update a Appointment' }),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: data_structures_1.AppointmentDto,
    }),
    (0, common_1.HttpCode)(200),
    tslib_1.__param(0, (0, common_1.Param)()),
    tslib_1.__param(1, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.AppointmentParamRequest,
        requests_1.AppointmentUpdateRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], AppointmentsController.prototype, "update", null);
tslib_1.__decorate([
    (0, common_1.Delete)('/:id'),
    (0, swagger_1.ApiOperation)({ summary: 'Delete a Appointment' }),
    (0, swagger_1.ApiResponse)({ status: 204 }),
    (0, common_1.HttpCode)(204),
    tslib_1.__param(0, (0, common_1.Param)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.AppointmentParamRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], AppointmentsController.prototype, "destroy", null);
AppointmentsController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('appointments'),
    (0, common_1.Controller)('/appointment'),
    tslib_1.__metadata("design:paramtypes", [cqrs_1.CommandBus,
        cqrs_1.QueryBus])
], AppointmentsController);
exports.AppointmentsController = AppointmentsController;
