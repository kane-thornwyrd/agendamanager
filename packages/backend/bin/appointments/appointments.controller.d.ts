import { CommandBus, QueryBus } from '@nestjs/cqrs';
import {
  PaginateAppointmentDto,
  AppointmentDto,
} from '@agendamanager/data-structures';
import {
  AppointmentCreateRequest,
  AppointmentPaginateRequest,
  AppointmentParamRequest,
  AppointmentUpdateRequest,
} from './requests';
export declare class AppointmentsController {
  private readonly commandBus;
  private readonly queryBus;
  constructor(commandBus: CommandBus, queryBus: QueryBus);
  index(
    AppointmentPaginate: AppointmentPaginateRequest
  ): Promise<PaginateAppointmentDto>;
  store(AppointmentCreate: AppointmentCreateRequest): Promise<AppointmentDto>;
  show({ id }: AppointmentParamRequest): Promise<AppointmentDto>;
  update(
    { id }: AppointmentParamRequest,
    AppointmentUpdate: AppointmentUpdateRequest
  ): Promise<AppointmentDto>;
  destroy({ id }: AppointmentParamRequest): Promise<void>;
}
