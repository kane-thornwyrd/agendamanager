"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseRequest = void 0;
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class BaseRequest {
}
tslib_1.__decorate([
    (0, class_validator_1.Allow)(),
    tslib_1.__metadata("design:type", Object)
], BaseRequest.prototype, "context", void 0);
exports.BaseRequest = BaseRequest;
