"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentUpdateRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const appointment_create_request_1 = require("./appointment-create.request");
class AppointmentUpdateRequest extends (0, swagger_1.PartialType)(appointment_create_request_1.AppointmentCreateRequest) {
}
exports.AppointmentUpdateRequest = AppointmentUpdateRequest;
