"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
tslib_1.__exportStar(require("./appointment-create.request"), exports);
tslib_1.__exportStar(require("./appointment-paginate.request"), exports);
tslib_1.__exportStar(require("./appointment-param.request"), exports);
tslib_1.__exportStar(require("./appointment-update.request"), exports);
