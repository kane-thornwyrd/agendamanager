"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentPaginateRequest = void 0;
const paginate_request_1 = require("../../infrastructure/db/typeorm/pagination/paginate.request");
class AppointmentPaginateRequest extends paginate_request_1.PaginateRequest {
}
exports.AppointmentPaginateRequest = AppointmentPaginateRequest;
