import { AppointmentCreateRequest } from './appointment-create.request';
declare const AppointmentUpdateRequest_base: import('@nestjs/common').Type<
  Partial<AppointmentCreateRequest>
>;
export declare class AppointmentUpdateRequest extends AppointmentUpdateRequest_base {}
export {};
