import { BaseRequest } from './base.request';
export declare class AppointmentCreateRequest extends BaseRequest {
  title: string;
  type: string;
  location: string;
  link: string;
  host: string;
  client: string;
  startTime: string;
  endTime: string;
}
