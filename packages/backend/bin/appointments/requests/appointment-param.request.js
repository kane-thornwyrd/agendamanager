"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentParamRequest = void 0;
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
const validations_1 = require("../validations");
const swagger_1 = require("@nestjs/swagger");
class AppointmentParamRequest {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNumber)(),
    (0, validations_1.AppointmentExists)(),
    tslib_1.__metadata("design:type", Number)
], AppointmentParamRequest.prototype, "id", void 0);
exports.AppointmentParamRequest = AppointmentParamRequest;
