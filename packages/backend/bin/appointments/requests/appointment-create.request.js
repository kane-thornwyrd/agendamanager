"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppointmentCreateRequest = void 0;
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
const base_request_1 = require("./base.request");
const swagger_1 = require("@nestjs/swagger");
class AppointmentCreateRequest extends base_request_1.BaseRequest {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(3),
    (0, class_validator_1.MaxLength)(255),
    tslib_1.__metadata("design:type", String)
], AppointmentCreateRequest.prototype, "title", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(3),
    (0, class_validator_1.MaxLength)(255),
    tslib_1.__metadata("design:type", String)
], AppointmentCreateRequest.prototype, "type", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    tslib_1.__metadata("design:type", String)
], AppointmentCreateRequest.prototype, "location", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({
        format: 'uri',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    tslib_1.__metadata("design:type", String)
], AppointmentCreateRequest.prototype, "link", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(3),
    tslib_1.__metadata("design:type", String)
], AppointmentCreateRequest.prototype, "host", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(3),
    tslib_1.__metadata("design:type", String)
], AppointmentCreateRequest.prototype, "client", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({
        format: 'date-time',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(3),
    tslib_1.__metadata("design:type", String)
], AppointmentCreateRequest.prototype, "startTime", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({
        format: 'date-time',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(3),
    tslib_1.__metadata("design:type", String)
], AppointmentCreateRequest.prototype, "endTime", void 0);
exports.AppointmentCreateRequest = AppointmentCreateRequest;
