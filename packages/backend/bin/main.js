"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const config_1 = require("@nestjs/config");
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const context_interceptor_1 = require("./infrastructure/context.interceptor");
const swagger_conf_1 = require("./infrastructure/swagger.conf");
const app_module_1 = require("./app.module");
/**
 * @returns {void}
 */
(async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    (0, class_validator_1.useContainer)(app.select(app_module_1.AppModule), { fallbackOnErrors: true });
    app.useGlobalPipes(new common_1.ValidationPipe({
        transform: true,
        whitelist: true,
    }));
    app.useGlobalInterceptors(new context_interceptor_1.ContextInterceptor());
    const configService = app.get(config_1.ConfigService);
    const document = swagger_1.SwaggerModule.createDocument(app, swagger_conf_1.swaggerConfig);
    swagger_1.SwaggerModule.setup('api', app, document);
    await app.listen(configService.get('PORT'));
})();
