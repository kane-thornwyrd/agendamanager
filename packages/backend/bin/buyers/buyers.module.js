"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyersModule = void 0;
const tslib_1 = require("tslib");
const data_structures_1 = require("@agendamanager/data-structures");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_splitter_module_1 = require("../infrastructure/db/typeorm/typeorm-splitter.module");
const handlers_1 = require("./commands/handlers");
const handlers_2 = require("./queries/handlers");
const buyers_repository_1 = require("./buyers.repository");
const validations_1 = require("./validations");
const buyers_controller_1 = require("./buyers.controller");
let BuyersModule = class BuyersModule {
};
BuyersModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [
            cqrs_1.CqrsModule,
            typeorm_1.TypeOrmModule.forFeature([data_structures_1.BuyerEntity]),
            typeorm_splitter_module_1.TypeOrmSplitterModule.forCustomRepository([buyers_repository_1.BuyersRepository]),
        ],
        controllers: [buyers_controller_1.BuyersController],
        providers: [validations_1.BuyerExistsValidation, ...handlers_1.CommandHandlers, ...handlers_2.QueryHandlers],
    })
], BuyersModule);
exports.BuyersModule = BuyersModule;
