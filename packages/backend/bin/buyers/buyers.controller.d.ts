import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { PaginateBuyerDto, BuyerDto } from '@agendamanager/data-structures';
import {
  BuyerCreateRequest,
  BuyerPaginateRequest,
  BuyerParamRequest,
  BuyerUpdateRequest,
} from './requests';
export declare class BuyersController {
  private readonly commandBus;
  private readonly queryBus;
  constructor(commandBus: CommandBus, queryBus: QueryBus);
  index(BuyerPaginate: BuyerPaginateRequest): Promise<PaginateBuyerDto>;
  store(BuyerCreate: BuyerCreateRequest): Promise<BuyerDto>;
  show({ id }: BuyerParamRequest): Promise<BuyerDto>;
  update(
    { id }: BuyerParamRequest,
    BuyerUpdate: BuyerUpdateRequest
  ): Promise<BuyerDto>;
  destroy({ id }: BuyerParamRequest): Promise<void>;
}
