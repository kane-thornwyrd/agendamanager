import { ICommandHandler } from '@nestjs/cqrs';
import { BuyerDto } from '@agendamanager/data-structures';
import { BuyersRepository } from '../../buyers.repository';
import { BuyerUpdateCommand } from '../buyer-update.command';
export declare class BuyerUpdateHandler
  implements ICommandHandler<BuyerUpdateCommand>
{
  private repository;
  constructor(repository: BuyersRepository);
  execute({ id, buyerUpdate }: BuyerUpdateCommand): Promise<BuyerDto>;
}
