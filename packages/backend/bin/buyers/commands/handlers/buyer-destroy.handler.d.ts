import { ICommandHandler } from '@nestjs/cqrs';
import { BuyersRepository } from '../../buyers.repository';
import { BuyerDestroyCommand } from '../buyer-destroy.command';
export declare class BuyerDestroyHandler
  implements ICommandHandler<BuyerDestroyCommand>
{
  private repository;
  constructor(repository: BuyersRepository);
  execute({ id }: BuyerDestroyCommand): Promise<void>;
}
