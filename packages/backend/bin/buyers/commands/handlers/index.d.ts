import { BuyerCreateHandler } from './buyer-create.handler';
import { BuyerUpdateHandler } from './buyer-update.handler';
import { BuyerDestroyHandler } from './buyer-destroy.handler';
export declare const CommandHandlers: (
  | typeof BuyerCreateHandler
  | typeof BuyerUpdateHandler
  | typeof BuyerDestroyHandler
)[];
