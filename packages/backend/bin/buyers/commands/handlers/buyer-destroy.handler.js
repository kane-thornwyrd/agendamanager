"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyerDestroyHandler = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const buyers_repository_1 = require("../../buyers.repository");
const buyer_destroy_command_1 = require("../buyer-destroy.command");
let BuyerDestroyHandler = class BuyerDestroyHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ id }) {
        try {
            const buyer = await this.repository.findOneBy({ id });
            await this.repository.remove(buyer);
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
BuyerDestroyHandler = tslib_1.__decorate([
    (0, cqrs_1.CommandHandler)(buyer_destroy_command_1.BuyerDestroyCommand),
    tslib_1.__metadata("design:paramtypes", [buyers_repository_1.BuyersRepository])
], BuyerDestroyHandler);
exports.BuyerDestroyHandler = BuyerDestroyHandler;
