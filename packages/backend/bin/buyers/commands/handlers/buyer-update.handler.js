"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyerUpdateHandler = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const class_transformer_1 = require("class-transformer");
const data_structures_1 = require("@agendamanager/data-structures");
const buyers_repository_1 = require("../../buyers.repository");
const buyer_update_command_1 = require("../buyer-update.command");
let BuyerUpdateHandler = class BuyerUpdateHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ id, buyerUpdate }) {
        try {
            const buyer = await this.repository.findOneBy({ id });
            Object.keys(buyerUpdate).forEach((key) => {
                const value = buyerUpdate[key];
                buyer[key] = value;
            });
            await this.repository.save(buyer);
            return (0, class_transformer_1.plainToClass)(data_structures_1.BuyerDto, buyer, { excludeExtraneousValues: true });
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
BuyerUpdateHandler = tslib_1.__decorate([
    (0, cqrs_1.CommandHandler)(buyer_update_command_1.BuyerUpdateCommand),
    tslib_1.__metadata("design:paramtypes", [buyers_repository_1.BuyersRepository])
], BuyerUpdateHandler);
exports.BuyerUpdateHandler = BuyerUpdateHandler;
