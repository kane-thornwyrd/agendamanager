import { ICommandHandler } from '@nestjs/cqrs';
import { BuyerDto } from '@agendamanager/data-structures';
import { BuyersRepository } from '../../buyers.repository';
import { BuyerCreateCommand } from '../buyer-create.command';
export declare class BuyerCreateHandler
  implements ICommandHandler<BuyerCreateCommand>
{
  private repository;
  constructor(repository: BuyersRepository);
  execute({
    BuyerCreate: { name, company },
  }: BuyerCreateCommand): Promise<BuyerDto>;
}
