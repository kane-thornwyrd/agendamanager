"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandHandlers = void 0;
const buyer_create_handler_1 = require("./buyer-create.handler");
const buyer_update_handler_1 = require("./buyer-update.handler");
const buyer_destroy_handler_1 = require("./buyer-destroy.handler");
exports.CommandHandlers = [
    buyer_create_handler_1.BuyerCreateHandler,
    buyer_update_handler_1.BuyerUpdateHandler,
    buyer_destroy_handler_1.BuyerDestroyHandler,
];
