"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyerCreateHandler = void 0;
const tslib_1 = require("tslib");
const cqrs_1 = require("@nestjs/cqrs");
const data_structures_1 = require("@agendamanager/data-structures");
const class_transformer_1 = require("class-transformer");
const common_1 = require("@nestjs/common");
const buyers_repository_1 = require("../../buyers.repository");
const buyer_create_command_1 = require("../buyer-create.command");
let BuyerCreateHandler = class BuyerCreateHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ BuyerCreate: { name, company }, }) {
        try {
            const newBuyer = this.repository.create();
            newBuyer.name = name;
            newBuyer.company = company;
            await this.repository.save(newBuyer);
            return (0, class_transformer_1.plainToClass)(data_structures_1.BuyerDto, newBuyer, {
                excludeExtraneousValues: true,
            });
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
BuyerCreateHandler = tslib_1.__decorate([
    (0, cqrs_1.CommandHandler)(buyer_create_command_1.BuyerCreateCommand),
    tslib_1.__metadata("design:paramtypes", [buyers_repository_1.BuyersRepository])
], BuyerCreateHandler);
exports.BuyerCreateHandler = BuyerCreateHandler;
