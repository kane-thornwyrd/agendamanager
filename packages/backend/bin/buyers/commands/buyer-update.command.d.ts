import { BuyerUpdateRequest } from '../requests';
export declare class BuyerUpdateCommand {
  readonly id: number;
  readonly buyerUpdate: BuyerUpdateRequest;
  constructor(id: number, buyerUpdate: BuyerUpdateRequest);
}
