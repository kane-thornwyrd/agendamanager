import { BuyerCreateRequest } from '../requests';
export declare class BuyerCreateCommand {
  readonly BuyerCreate: BuyerCreateRequest;
  constructor(BuyerCreate: BuyerCreateRequest);
}
