"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyerUpdateCommand = void 0;
class BuyerUpdateCommand {
    constructor(id, buyerUpdate) {
        this.id = id;
        this.buyerUpdate = buyerUpdate;
    }
}
exports.BuyerUpdateCommand = BuyerUpdateCommand;
