"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyerDestroyCommand = void 0;
class BuyerDestroyCommand {
    constructor(id) {
        this.id = id;
    }
}
exports.BuyerDestroyCommand = BuyerDestroyCommand;
