import { BuyerEntity } from '@agendamanager/data-structures';
import { BaseRepository } from '../infrastructure/db/typeorm/base.repository';
export declare class BuyersRepository extends BaseRepository<BuyerEntity> {}
