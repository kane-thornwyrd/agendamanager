"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyersController = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const swagger_1 = require("@nestjs/swagger");
const data_structures_1 = require("@agendamanager/data-structures");
const commands_1 = require("./commands");
const queries_1 = require("./queries");
const requests_1 = require("./requests");
let BuyersController = class BuyersController {
    constructor(commandBus, queryBus) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
    }
    async index(BuyerPaginate) {
        return this.queryBus.execute(new queries_1.BuyerPaginateQuery(BuyerPaginate));
    }
    async store(BuyerCreate) {
        return this.commandBus.execute(new commands_1.BuyerCreateCommand(BuyerCreate));
    }
    async show({ id }) {
        return this.queryBus.execute(new queries_1.BuyerShowQuery(+id));
    }
    async update({ id }, BuyerUpdate) {
        return this.commandBus.execute(new commands_1.BuyerUpdateCommand(id, BuyerUpdate));
    }
    async destroy({ id }) {
        await this.commandBus.execute(new commands_1.BuyerDestroyCommand(id));
    }
};
tslib_1.__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiOperation)({ summary: 'buyers Paginated' }),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: data_structures_1.PaginateBuyerDto,
    }),
    tslib_1.__param(0, (0, common_1.Query)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.BuyerPaginateRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], BuyersController.prototype, "index", null);
tslib_1.__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create a new Buyer' }),
    (0, swagger_1.ApiResponse)({
        status: 201,
        type: data_structures_1.BuyerDto,
    }),
    (0, common_1.HttpCode)(201),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.BuyerCreateRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], BuyersController.prototype, "store", null);
tslib_1.__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiOperation)({ summary: 'Show a Buyer' }),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: data_structures_1.BuyerDto,
    }),
    (0, common_1.HttpCode)(200),
    tslib_1.__param(0, (0, common_1.Param)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.BuyerParamRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], BuyersController.prototype, "show", null);
tslib_1.__decorate([
    (0, common_1.Put)('/:id'),
    (0, swagger_1.ApiOperation)({ summary: 'Update a Buyer' }),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: data_structures_1.BuyerDto,
    }),
    (0, common_1.HttpCode)(200),
    tslib_1.__param(0, (0, common_1.Param)()),
    tslib_1.__param(1, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.BuyerParamRequest,
        requests_1.BuyerUpdateRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], BuyersController.prototype, "update", null);
tslib_1.__decorate([
    (0, common_1.Delete)('/:id'),
    (0, swagger_1.ApiOperation)({ summary: 'Delete a Buyer' }),
    (0, swagger_1.ApiResponse)({ status: 204 }),
    (0, common_1.HttpCode)(204),
    tslib_1.__param(0, (0, common_1.Param)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.BuyerParamRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], BuyersController.prototype, "destroy", null);
BuyersController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('buyers'),
    (0, common_1.Controller)('/buyer'),
    tslib_1.__metadata("design:paramtypes", [cqrs_1.CommandBus,
        cqrs_1.QueryBus])
], BuyersController);
exports.BuyersController = BuyersController;
