import { BaseRequest } from './base.request';
export declare class BuyerCreateRequest extends BaseRequest {
  name: string;
  company: string;
}
