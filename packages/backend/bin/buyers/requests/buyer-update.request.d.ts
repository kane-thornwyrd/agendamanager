import { BuyerCreateRequest } from './buyer-create.request';
declare const BuyerUpdateRequest_base: import('@nestjs/common').Type<
  Partial<BuyerCreateRequest>
>;
export declare class BuyerUpdateRequest extends BuyerUpdateRequest_base {}
export {};
