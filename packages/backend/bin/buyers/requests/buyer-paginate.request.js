"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyerPaginateRequest = void 0;
const paginate_request_1 = require("../../infrastructure/db/typeorm/pagination/paginate.request");
class BuyerPaginateRequest extends paginate_request_1.PaginateRequest {
}
exports.BuyerPaginateRequest = BuyerPaginateRequest;
