"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyerUpdateRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const buyer_create_request_1 = require("./buyer-create.request");
class BuyerUpdateRequest extends (0, swagger_1.PartialType)(buyer_create_request_1.BuyerCreateRequest) {
}
exports.BuyerUpdateRequest = BuyerUpdateRequest;
