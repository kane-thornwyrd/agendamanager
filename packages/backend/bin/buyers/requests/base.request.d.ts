export declare class BaseRequest {
  context?: {
    params: {
      id?: number;
    };
    query: any;
    user: any;
  };
}
