"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyerCreateRequest = void 0;
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
const base_request_1 = require("./base.request");
const swagger_1 = require("@nestjs/swagger");
class BuyerCreateRequest extends base_request_1.BaseRequest {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(3),
    (0, class_validator_1.MaxLength)(255),
    tslib_1.__metadata("design:type", String)
], BuyerCreateRequest.prototype, "name", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.MinLength)(3),
    (0, class_validator_1.MaxLength)(255),
    tslib_1.__metadata("design:type", String)
], BuyerCreateRequest.prototype, "company", void 0);
exports.BuyerCreateRequest = BuyerCreateRequest;
