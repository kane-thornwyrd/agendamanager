import {
  ValidationOptions,
  ValidatorConstraintInterface,
} from 'class-validator';
import { BuyersRepository } from '../buyers.repository';
export declare class BuyerExistsValidation
  implements ValidatorConstraintInterface
{
  private repository;
  constructor(repository: BuyersRepository);
  validate(id: string): Promise<boolean>;
  defaultMessage(): string;
}
/**
 *
 * @param {object} validationOptions not much in this case
 * @returns {function} a factory for a custom validator decorator
 */
export declare function BuyerExists(
  validationOptions?: ValidationOptions
): (object: any, propertyName: string) => void;
