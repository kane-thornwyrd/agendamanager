"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyerExists = exports.BuyerExistsValidation = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const buyers_repository_1 = require("../buyers.repository");
let BuyerExistsValidation = class BuyerExistsValidation {
    constructor(repository) {
        this.repository = repository;
    }
    async validate(id) {
        try {
            const buyer = await this.repository.findOneBy({ id: +id });
            return !!buyer;
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
    defaultMessage() {
        return `Buyer does not exists.`;
    }
};
BuyerExistsValidation = tslib_1.__decorate([
    (0, class_validator_1.ValidatorConstraint)({ name: 'buyerExists', async: true }),
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [buyers_repository_1.BuyersRepository])
], BuyerExistsValidation);
exports.BuyerExistsValidation = BuyerExistsValidation;
/**
 *
 * @param {object} validationOptions not much in this case
 * @returns {function} a factory for a custom validator decorator
 */
function BuyerExists(validationOptions) {
    return function (object, propertyName) {
        (0, class_validator_1.registerDecorator)({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            validator: BuyerExistsValidation,
        });
    };
}
exports.BuyerExists = BuyerExists;
