"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyerPaginateQuery = void 0;
class BuyerPaginateQuery {
    constructor(buyerPaginate) {
        this.buyerPaginate = buyerPaginate;
    }
}
exports.BuyerPaginateQuery = BuyerPaginateQuery;
