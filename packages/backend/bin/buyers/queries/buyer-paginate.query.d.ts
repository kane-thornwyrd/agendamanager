import { BuyerPaginateRequest } from '../requests';
export declare class BuyerPaginateQuery {
  readonly buyerPaginate: BuyerPaginateRequest;
  constructor(buyerPaginate: BuyerPaginateRequest);
}
