"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyerShowHandler = exports.BuyerPaginateHandler = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const class_transformer_1 = require("class-transformer");
const data_structures_1 = require("@agendamanager/data-structures");
const buyers_repository_1 = require("../../buyers.repository");
const buyer_paginate_query_1 = require("../buyer-paginate.query");
const buyer_show_query_1 = require("../buyer-show.query");
const data_structures_2 = require("@agendamanager/data-structures");
let BuyerPaginateHandler = class BuyerPaginateHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ buyerPaginate, }) {
        const result = await this.repository.paginate(buyerPaginate);
        return (0, class_transformer_1.plainToClass)(data_structures_1.PaginateBuyerDto, result, {
            excludeExtraneousValues: true,
        });
    }
};
BuyerPaginateHandler = tslib_1.__decorate([
    (0, cqrs_1.QueryHandler)(buyer_paginate_query_1.BuyerPaginateQuery),
    tslib_1.__metadata("design:paramtypes", [buyers_repository_1.BuyersRepository])
], BuyerPaginateHandler);
exports.BuyerPaginateHandler = BuyerPaginateHandler;
let BuyerShowHandler = class BuyerShowHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ id }) {
        try {
            const buyer = await this.repository.findOneBy({ id });
            return (0, class_transformer_1.plainToClass)(data_structures_2.BuyerDto, buyer, { excludeExtraneousValues: true });
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
BuyerShowHandler = tslib_1.__decorate([
    (0, cqrs_1.QueryHandler)(buyer_show_query_1.BuyerShowQuery),
    tslib_1.__metadata("design:paramtypes", [buyers_repository_1.BuyersRepository])
], BuyerShowHandler);
exports.BuyerShowHandler = BuyerShowHandler;
