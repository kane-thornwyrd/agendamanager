"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryHandlers = void 0;
const buyers_handler_1 = require("./buyers.handler");
exports.QueryHandlers = [buyers_handler_1.BuyerPaginateHandler, buyers_handler_1.BuyerShowHandler];
