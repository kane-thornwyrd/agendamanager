import { BuyerPaginateHandler, BuyerShowHandler } from './buyers.handler';
export declare const QueryHandlers: (
  | typeof BuyerPaginateHandler
  | typeof BuyerShowHandler
)[];
