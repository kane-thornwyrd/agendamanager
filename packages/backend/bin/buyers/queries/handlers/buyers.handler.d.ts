import { IQueryHandler } from '@nestjs/cqrs';
import { PaginateBuyerDto } from '@agendamanager/data-structures';
import { BuyersRepository } from '../../buyers.repository';
import { BuyerPaginateQuery } from '../buyer-paginate.query';
import { BuyerShowQuery } from '../buyer-show.query';
import { BuyerDto } from '@agendamanager/data-structures';
export declare class BuyerPaginateHandler
  implements IQueryHandler<BuyerPaginateQuery>
{
  private repository;
  constructor(repository: BuyersRepository);
  execute({ buyerPaginate }: BuyerPaginateQuery): Promise<PaginateBuyerDto>;
}
export declare class BuyerShowHandler implements IQueryHandler<BuyerShowQuery> {
  private repository;
  constructor(repository: BuyersRepository);
  execute({ id }: BuyerShowQuery): Promise<BuyerDto>;
}
