"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContextInterceptor = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
let ContextInterceptor = class ContextInterceptor {
    intercept(context, next) {
        const request = context.switchToHttp().getRequest();
        request.body.context = {
            params: request.params,
            query: request.query,
        };
        return next.handle();
    }
};
ContextInterceptor = tslib_1.__decorate([
    (0, common_1.Injectable)()
], ContextInterceptor);
exports.ContextInterceptor = ContextInterceptor;
