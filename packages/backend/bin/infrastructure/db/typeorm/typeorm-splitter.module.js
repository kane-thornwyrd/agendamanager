"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypeOrmSplitterModule = void 0;
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_splitter_decorator_1 = require("./typeorm-splitter.decorator");
/**
 * Module that will use the metadata `TYPEORM_SPLITTER_CUSTOM_REPOSITORY` to
 * get the correct config to use a specific repo for a specific type of entity.
 * @returns {DynamicModule} Module to import (put in the `imports` array) for a
 * CQRS-style module to inject the custom repository for the target module entity.
 *
 * @example
 * [at]Module({
 *   imports: [
 *     CqrsModule,
 *     TypeOrmModule.forFeature([MyEntity]),
 *     TypeOrmSplitterModule.forCustomRepository([MyRepository]),
 *   ],
 *   controllers: [MyController],
 *   providers: [
 *     ...MyValidation1,
 *     ...MyValidation2,
 *     ...MyValidationX,
 *     ...CommandHandlers,
 *     ...QueryHandlers,
 *   ],
 * })
 * export class MyModule {}
 *
 **/
class TypeOrmSplitterModule {
    static forCustomRepository(repositories) {
        const providers = [];
        for (const repository of repositories) {
            const entity = Reflect.getMetadata(typeorm_splitter_decorator_1.TYPEORM_SPLITTER_CUSTOM_REPOSITORY, repository);
            if (!entity) {
                continue;
            }
            providers.push({
                inject: [(0, typeorm_1.getDataSourceToken)()],
                provide: repository,
                useFactory: (dataSource) => {
                    const baseRepository = dataSource.getRepository(entity);
                    return new repository(baseRepository.target, baseRepository.manager, baseRepository.queryRunner);
                },
            });
        }
        return {
            exports: providers,
            module: TypeOrmSplitterModule,
            providers,
        };
    }
}
exports.TypeOrmSplitterModule = TypeOrmSplitterModule;
