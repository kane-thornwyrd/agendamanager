"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
const typeorm_1 = require("typeorm");
dotenv.config();
const typeOrmConfig = new typeorm_1.DataSource({
    type: process.env.DATABASE_CONNECTION_TYPE,
    database: process.env.DATABASE_FILE,
    entities: [`${__dirname}/../../../**/*.entity{.js,.ts}`],
    synchronize: false,
    logging: true,
    migrations: [`${__dirname}/../../../migrations/*{.js,.ts}`],
});
exports.default = typeOrmConfig;
