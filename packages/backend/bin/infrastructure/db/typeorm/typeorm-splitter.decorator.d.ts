export declare const TYPEORM_SPLITTER_CUSTOM_REPOSITORY =
  'TYPEORM_SPLITTER_CUSTOM_REPOSITORY';
/**
 * Produce a decorator that set the metadata `TYPEORM_SPLITTER_CUSTOM_REPOSITORY`
 * to the entity that need a custom repos.
 *
 * Useless alone, but part of the process.
 * @param {any} entity the kind of entity that need a custom repo.
 * @returns {CustomDecorator<string>} Decorator that assigns metadata to the
 *    class/function using the `TYPEORM_SPLITTER_CUSTOM_REPOSITORY` key.
 */
export declare function CustomRepository(entity: any): ClassDecorator;
