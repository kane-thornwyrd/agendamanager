"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomRepository = exports.TYPEORM_SPLITTER_CUSTOM_REPOSITORY = void 0;
const common_1 = require("@nestjs/common");
exports.TYPEORM_SPLITTER_CUSTOM_REPOSITORY = 'TYPEORM_SPLITTER_CUSTOM_REPOSITORY';
/**
 * Produce a decorator that set the metadata `TYPEORM_SPLITTER_CUSTOM_REPOSITORY`
 * to the entity that need a custom repos.
 *
 * Useless alone, but part of the process.
 * @param {any} entity the kind of entity that need a custom repo.
 * @returns {CustomDecorator<string>} Decorator that assigns metadata to the
 *    class/function using the `TYPEORM_SPLITTER_CUSTOM_REPOSITORY` key.
 */
function CustomRepository(entity) {
    return (0, common_1.SetMetadata)(exports.TYPEORM_SPLITTER_CUSTOM_REPOSITORY, entity);
}
exports.CustomRepository = CustomRepository;
