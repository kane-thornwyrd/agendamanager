import { FindOptionsSelect, Repository } from 'typeorm';
import { PaginateRequest } from './pagination/paginate.request';
import { PaginateDto } from './pagination/paginate.dto';
export declare abstract class BaseRepository<T> extends Repository<T> {
  paginate(
    paginateDto: PaginateRequest,
    select?: FindOptionsSelect<T>,
    relations?: string[]
  ): Promise<PaginateDto<T>>;
}
