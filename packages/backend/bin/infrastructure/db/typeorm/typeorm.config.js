"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.typeOrmAsyncConfig = void 0;
const config_1 = require("@nestjs/config");
const PATH = require("path");
console.log(PATH.resolve(`${__dirname}/../../../**/*.entity{.js,.ts}`));
exports.typeOrmAsyncConfig = {
    useFactory: (configService) => configService.get('NODE_ENV') !== 'test'
        ? {
            type: configService.get('DATABASE_CONNECTION_TYPE'),
            database: configService.get('DATABASE_FILE'),
            entities: [`${__dirname}/../../../**/*.entity{.js,.ts}`],
            synchronize: false,
            logging: configService.get('NODE_ENV') !== 'production'
                ? true
                : false,
        }
        : {
            type: 'sqlite',
            database: ':memory:',
            entities: [`${__dirname}/../../../**/*.entity{.js,.ts}`],
            synchronize: true,
            logging: false,
            dropSchema: true,
        },
    inject: [config_1.ConfigService],
};
