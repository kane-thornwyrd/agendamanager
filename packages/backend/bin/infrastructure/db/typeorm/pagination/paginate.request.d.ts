export declare class PaginateRequest {
  search: string;
  type: string;
  page: number;
  sort: string;
  perPage: number;
}
