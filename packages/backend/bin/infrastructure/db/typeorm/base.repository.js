"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseRepository = void 0;
const typeorm_1 = require("typeorm");
class BaseRepository extends typeorm_1.Repository {
    async paginate(paginateDto, select = {}, relations = []) {
        const { page, perPage, search, type, sort } = paginateDto;
        const take = perPage || 15;
        const skip = ((page || 1) - 1) * take;
        let order = sort ? sort : 'id';
        if (order.charAt(0) === '-') {
            order = order.substring(1);
        }
        let direction = 'ASC';
        if (sort && sort.charAt(0) === '-') {
            direction = 'DESC';
        }
        let where = {};
        if (search && type) {
            where = {
                [type]: (0, typeorm_1.ILike)(`%${search}%`),
            };
        }
        const [result, total] = await this.findAndCount({
            select,
            where,
            take,
            skip,
            order: {
                [order]: direction,
            },
            relations,
        });
        return {
            data: result,
            search,
            type,
            sort,
            page: page || 1,
            perPage: perPage || 15,
            total,
        };
    }
}
exports.BaseRepository = BaseRepository;
