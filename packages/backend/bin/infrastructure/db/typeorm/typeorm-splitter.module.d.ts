import { DynamicModule } from '@nestjs/common';
/**
 * Module that will use the metadata `TYPEORM_SPLITTER_CUSTOM_REPOSITORY` to
 * get the correct config to use a specific repo for a specific type of entity.
 * @returns {DynamicModule} Module to import (put in the `imports` array) for a
 * CQRS-style module to inject the custom repository for the target module entity.
 *
 * @example
 * [at]Module({
 *   imports: [
 *     CqrsModule,
 *     TypeOrmModule.forFeature([MyEntity]),
 *     TypeOrmSplitterModule.forCustomRepository([MyRepository]),
 *   ],
 *   controllers: [MyController],
 *   providers: [
 *     ...MyValidation1,
 *     ...MyValidation2,
 *     ...MyValidationX,
 *     ...CommandHandlers,
 *     ...QueryHandlers,
 *   ],
 * })
 * export class MyModule {}
 *
 **/
export declare class TypeOrmSplitterModule {
  static forCustomRepository<T extends new (...args: any[]) => any>(
    repositories: T[]
  ): DynamicModule;
}
