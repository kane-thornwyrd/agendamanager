"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.migrations1671406097589 = void 0;
class migrations1671406097589 {
    constructor() {
        this.name = 'migrations1671406097589';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "buyer" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255) NOT NULL, "company" varchar(255) NOT NULL)`);
        await queryRunner.query(`CREATE TABLE "vendor" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255) NOT NULL)`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "vendor"`);
        await queryRunner.query(`DROP TABLE "buyer"`);
    }
}
exports.migrations1671406097589 = migrations1671406097589;
