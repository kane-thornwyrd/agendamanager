"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.migrations1671416658173 = void 0;
class migrations1671416658173 {
    constructor() {
        this.name = 'migrations1671416658173';
    }
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "appointment" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar(255) NOT NULL, "type" varchar NOT NULL DEFAULT ('physical'), "location" text, "link" text, "host" varchar(255) NOT NULL, "client" varchar(255) NOT NULL, "startTime" datetime NOT NULL, "endTime" datetime NOT NULL)`);
        await queryRunner.query(`CREATE TABLE "buyer" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255) NOT NULL, "company" varchar(255) NOT NULL)`);
        await queryRunner.query(`CREATE TABLE "vendor" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255) NOT NULL)`);
    }
    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "vendor"`);
        await queryRunner.query(`DROP TABLE "buyer"`);
        await queryRunner.query(`DROP TABLE "appointment"`);
    }
}
exports.migrations1671416658173 = migrations1671416658173;
