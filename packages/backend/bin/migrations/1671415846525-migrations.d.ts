import { MigrationInterface, QueryRunner } from 'typeorm';
export declare class migrations1671415846525 implements MigrationInterface {
  name: string;
  up(queryRunner: QueryRunner): Promise<void>;
  down(queryRunner: QueryRunner): Promise<void>;
}
