"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const Joi = require("joi");
const db_module_1 = require("./infrastructure/db/db.module");
const appointments_module_1 = require("./appointments/appointments.module");
const vendors_module_1 = require("./vendors/vendors.module");
const buyers_module_1 = require("./buyers/buyers.module");
let AppModule = class AppModule {
};
AppModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                isGlobal: true,
                validationSchema: Joi.object({
                    PORT: Joi.number().required(),
                    DATABASE_CONNECTION_TYPE: Joi.string().required(),
                    DATABASE_FILE: Joi.string().required(),
                }),
            }),
            db_module_1.DatabaseModule,
            appointments_module_1.AppointmentsModule,
            buyers_module_1.BuyersModule,
            vendors_module_1.VendorsModule,
        ],
        controllers: [],
        providers: [],
    })
], AppModule);
exports.AppModule = AppModule;
