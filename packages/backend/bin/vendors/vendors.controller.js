"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorsController = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const swagger_1 = require("@nestjs/swagger");
const data_structures_1 = require("@agendamanager/data-structures");
const commands_1 = require("./commands");
const queries_1 = require("./queries");
const requests_1 = require("./requests");
let VendorsController = class VendorsController {
    constructor(commandBus, queryBus) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
    }
    async index(vendorPaginate) {
        return this.queryBus.execute(new queries_1.VendorPaginateQuery(vendorPaginate));
    }
    async store(vendorCreate) {
        return this.commandBus.execute(new commands_1.VendorCreateCommand(vendorCreate));
    }
    async show({ id }) {
        return this.queryBus.execute(new queries_1.VendorShowQuery(parseInt(id, 10)));
    }
    async update({ id }, vendorUpdate) {
        return this.commandBus.execute(new commands_1.VendorUpdateCommand(id, vendorUpdate));
    }
    async destroy({ id }) {
        await this.commandBus.execute(new commands_1.VendorDestroyCommand(id));
    }
};
tslib_1.__decorate([
    (0, common_1.Get)('/list'),
    (0, swagger_1.ApiOperation)({ summary: 'Vendors Paginated' }),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: data_structures_1.PaginateVendorDto,
    }),
    tslib_1.__param(0, (0, common_1.Query)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.VendorPaginateRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], VendorsController.prototype, "index", null);
tslib_1.__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create a new Vendor' }),
    (0, swagger_1.ApiResponse)({
        status: 201,
        type: data_structures_1.VendorDto,
    }),
    (0, common_1.HttpCode)(201),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.VendorCreateRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], VendorsController.prototype, "store", null);
tslib_1.__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiOperation)({ summary: 'Show a Vendor' }),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: data_structures_1.VendorDto,
    }),
    (0, common_1.HttpCode)(200),
    tslib_1.__param(0, (0, common_1.Param)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.VendorParamRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], VendorsController.prototype, "show", null);
tslib_1.__decorate([
    (0, common_1.Put)('/:id'),
    (0, swagger_1.ApiOperation)({ summary: 'Update a Vendor' }),
    (0, swagger_1.ApiResponse)({
        status: 200,
        type: data_structures_1.VendorDto,
    }),
    (0, common_1.HttpCode)(200),
    tslib_1.__param(0, (0, common_1.Param)()),
    tslib_1.__param(1, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.VendorParamRequest,
        requests_1.VendorUpdateRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], VendorsController.prototype, "update", null);
tslib_1.__decorate([
    (0, common_1.Delete)('/:id'),
    (0, swagger_1.ApiOperation)({ summary: 'Delete a Vendor' }),
    (0, swagger_1.ApiResponse)({ status: 204 }),
    (0, common_1.HttpCode)(204),
    tslib_1.__param(0, (0, common_1.Param)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [requests_1.VendorParamRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], VendorsController.prototype, "destroy", null);
VendorsController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('vendors'),
    (0, common_1.Controller)('/vendor'),
    tslib_1.__metadata("design:paramtypes", [cqrs_1.CommandBus,
        cqrs_1.QueryBus])
], VendorsController);
exports.VendorsController = VendorsController;
