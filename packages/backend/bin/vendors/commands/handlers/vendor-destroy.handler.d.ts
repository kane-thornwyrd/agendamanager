import { ICommandHandler } from '@nestjs/cqrs';
import { VendorsRepository } from '../../vendor.repository';
import { VendorDestroyCommand } from '../vendor-destroy.command';
export declare class VendorDestroyHandler
  implements ICommandHandler<VendorDestroyCommand>
{
  private repository;
  constructor(repository: VendorsRepository);
  execute({ id }: VendorDestroyCommand): Promise<void>;
}
