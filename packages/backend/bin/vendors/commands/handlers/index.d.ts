import { VendorCreateHandler } from './vendor-create.handler';
import { VendorUpdateHandler } from './vendor-update.handler';
import { VendorDestroyHandler } from './vendor-destroy.handler';
export declare const CommandHandlers: (
  | typeof VendorCreateHandler
  | typeof VendorUpdateHandler
  | typeof VendorDestroyHandler
)[];
