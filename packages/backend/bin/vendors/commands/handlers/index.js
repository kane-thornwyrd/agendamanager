"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommandHandlers = void 0;
const vendor_create_handler_1 = require("./vendor-create.handler");
const vendor_update_handler_1 = require("./vendor-update.handler");
const vendor_destroy_handler_1 = require("./vendor-destroy.handler");
exports.CommandHandlers = [
    vendor_create_handler_1.VendorCreateHandler,
    vendor_update_handler_1.VendorUpdateHandler,
    vendor_destroy_handler_1.VendorDestroyHandler,
];
