import { ICommandHandler } from '@nestjs/cqrs';
import { VendorDto } from '@agendamanager/data-structures';
import { VendorsRepository } from '../../vendor.repository';
import { VendorUpdateCommand } from '../vendor-update.command';
export declare class VendorUpdateHandler
  implements ICommandHandler<VendorUpdateCommand>
{
  private repository;
  constructor(repository: VendorsRepository);
  execute({ id, vendorUpdate }: VendorUpdateCommand): Promise<VendorDto>;
}
