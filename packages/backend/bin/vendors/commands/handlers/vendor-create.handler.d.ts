import { ICommandHandler } from '@nestjs/cqrs';
import { VendorDto } from '@agendamanager/data-structures';
import { VendorsRepository } from '../../vendor.repository';
import { VendorCreateCommand } from '../vendor-create.command';
export declare class VendorCreateHandler
  implements ICommandHandler<VendorCreateCommand>
{
  private repository;
  constructor(repository: VendorsRepository);
  execute({ vendorCreate: { name } }: VendorCreateCommand): Promise<VendorDto>;
}
