"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorDestroyHandler = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const vendor_repository_1 = require("../../vendor.repository");
const vendor_destroy_command_1 = require("../vendor-destroy.command");
let VendorDestroyHandler = class VendorDestroyHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ id }) {
        try {
            const vendor = await this.repository.findOneBy({ id: +id });
            await this.repository.remove(vendor);
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
VendorDestroyHandler = tslib_1.__decorate([
    (0, cqrs_1.CommandHandler)(vendor_destroy_command_1.VendorDestroyCommand),
    tslib_1.__metadata("design:paramtypes", [vendor_repository_1.VendorsRepository])
], VendorDestroyHandler);
exports.VendorDestroyHandler = VendorDestroyHandler;
