"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorUpdateHandler = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const class_transformer_1 = require("class-transformer");
const data_structures_1 = require("@agendamanager/data-structures");
const vendor_repository_1 = require("../../vendor.repository");
const vendor_update_command_1 = require("../vendor-update.command");
let VendorUpdateHandler = class VendorUpdateHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ id, vendorUpdate }) {
        try {
            const vendor = await this.repository.findOneBy({ id: +id });
            Object.keys(vendorUpdate).forEach((key) => {
                const value = vendorUpdate[key];
                vendor[key] = value;
            });
            await this.repository.save(vendor);
            return (0, class_transformer_1.plainToClass)(data_structures_1.VendorDto, vendor, { excludeExtraneousValues: true });
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
VendorUpdateHandler = tslib_1.__decorate([
    (0, cqrs_1.CommandHandler)(vendor_update_command_1.VendorUpdateCommand),
    tslib_1.__metadata("design:paramtypes", [vendor_repository_1.VendorsRepository])
], VendorUpdateHandler);
exports.VendorUpdateHandler = VendorUpdateHandler;
