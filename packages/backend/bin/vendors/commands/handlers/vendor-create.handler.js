"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorCreateHandler = void 0;
const tslib_1 = require("tslib");
const cqrs_1 = require("@nestjs/cqrs");
const data_structures_1 = require("@agendamanager/data-structures");
const vendor_repository_1 = require("../../vendor.repository");
const common_1 = require("@nestjs/common");
const class_transformer_1 = require("class-transformer");
const vendor_create_command_1 = require("../vendor-create.command");
let VendorCreateHandler = class VendorCreateHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ vendorCreate: { name }, }) {
        try {
            const newVendor = this.repository.create();
            newVendor.name = name;
            await this.repository.save(newVendor);
            return (0, class_transformer_1.plainToClass)(data_structures_1.VendorDto, newVendor, {
                excludeExtraneousValues: true,
            });
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
VendorCreateHandler = tslib_1.__decorate([
    (0, cqrs_1.CommandHandler)(vendor_create_command_1.VendorCreateCommand),
    tslib_1.__metadata("design:paramtypes", [vendor_repository_1.VendorsRepository])
], VendorCreateHandler);
exports.VendorCreateHandler = VendorCreateHandler;
