"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorUpdateCommand = void 0;
class VendorUpdateCommand {
    constructor(id, vendorUpdate) {
        this.id = id;
        this.vendorUpdate = vendorUpdate;
    }
}
exports.VendorUpdateCommand = VendorUpdateCommand;
