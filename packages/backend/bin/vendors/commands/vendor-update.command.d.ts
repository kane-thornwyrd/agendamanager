import { VendorUpdateRequest } from '../requests';
export declare class VendorUpdateCommand {
  readonly id: string;
  readonly vendorUpdate: VendorUpdateRequest;
  constructor(id: string, vendorUpdate: VendorUpdateRequest);
}
