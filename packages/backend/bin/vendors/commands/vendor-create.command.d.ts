import { VendorCreateRequest } from '../requests';
export declare class VendorCreateCommand {
  readonly vendorCreate: VendorCreateRequest;
  constructor(vendorCreate: VendorCreateRequest);
}
