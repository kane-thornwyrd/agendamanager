"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorDestroyCommand = void 0;
class VendorDestroyCommand {
    constructor(id) {
        this.id = id;
    }
}
exports.VendorDestroyCommand = VendorDestroyCommand;
