"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorCreateCommand = void 0;
class VendorCreateCommand {
    constructor(vendorCreate) {
        this.vendorCreate = vendorCreate;
    }
}
exports.VendorCreateCommand = VendorCreateCommand;
