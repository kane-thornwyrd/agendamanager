import { VendorEntity } from '@agendamanager/data-structures';
import { BaseRepository } from '../infrastructure/db/typeorm/base.repository';
export declare class VendorsRepository extends BaseRepository<VendorEntity> {}
