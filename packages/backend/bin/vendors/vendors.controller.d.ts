import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { PaginateVendorDto, VendorDto } from '@agendamanager/data-structures';
import {
  VendorCreateRequest,
  VendorPaginateRequest,
  VendorParamRequest,
  VendorUpdateRequest,
} from './requests';
export declare class VendorsController {
  private readonly commandBus;
  private readonly queryBus;
  constructor(commandBus: CommandBus, queryBus: QueryBus);
  index(vendorPaginate: VendorPaginateRequest): Promise<PaginateVendorDto>;
  store(vendorCreate: VendorCreateRequest): Promise<VendorDto>;
  show({ id }: VendorParamRequest): Promise<VendorDto>;
  update(
    { id }: VendorParamRequest,
    vendorUpdate: VendorUpdateRequest
  ): Promise<VendorDto>;
  destroy({ id }: VendorParamRequest): Promise<void>;
}
