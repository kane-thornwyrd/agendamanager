"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorsRepository = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const data_structures_1 = require("@agendamanager/data-structures");
const base_repository_1 = require("../infrastructure/db/typeorm/base.repository");
const typeorm_splitter_decorator_1 = require("../infrastructure/db/typeorm/typeorm-splitter.decorator");
let VendorsRepository = class VendorsRepository extends base_repository_1.BaseRepository {
};
VendorsRepository = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    (0, typeorm_splitter_decorator_1.CustomRepository)(data_structures_1.VendorEntity)
], VendorsRepository);
exports.VendorsRepository = VendorsRepository;
