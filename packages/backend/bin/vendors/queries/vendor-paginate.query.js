"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorPaginateQuery = void 0;
class VendorPaginateQuery {
    constructor(vendorPaginate) {
        this.vendorPaginate = vendorPaginate;
    }
}
exports.VendorPaginateQuery = VendorPaginateQuery;
