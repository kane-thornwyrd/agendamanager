import { VendorPaginateRequest } from '../requests';
export declare class VendorPaginateQuery {
  readonly vendorPaginate: VendorPaginateRequest;
  constructor(vendorPaginate: VendorPaginateRequest);
}
