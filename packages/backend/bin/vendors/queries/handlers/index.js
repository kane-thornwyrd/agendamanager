"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryHandlers = void 0;
const vendors_handler_1 = require("./vendors.handler");
exports.QueryHandlers = [vendors_handler_1.VendorPaginateHandler, vendors_handler_1.VendorShowHandler];
