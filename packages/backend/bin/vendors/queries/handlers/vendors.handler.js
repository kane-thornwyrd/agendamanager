"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorShowHandler = exports.VendorPaginateHandler = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const class_transformer_1 = require("class-transformer");
const data_structures_1 = require("@agendamanager/data-structures");
const vendor_repository_1 = require("../../vendor.repository");
const vendor_paginate_query_1 = require("../vendor-paginate.query");
const vendor_show_query_1 = require("../vendor-show.query");
const data_structures_2 = require("@agendamanager/data-structures");
let VendorPaginateHandler = class VendorPaginateHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ vendorPaginate, }) {
        const result = await this.repository.paginate(vendorPaginate);
        return (0, class_transformer_1.plainToClass)(data_structures_1.PaginateVendorDto, result, {
            excludeExtraneousValues: true,
        });
    }
};
VendorPaginateHandler = tslib_1.__decorate([
    (0, cqrs_1.QueryHandler)(vendor_paginate_query_1.VendorPaginateQuery),
    tslib_1.__metadata("design:paramtypes", [vendor_repository_1.VendorsRepository])
], VendorPaginateHandler);
exports.VendorPaginateHandler = VendorPaginateHandler;
let VendorShowHandler = class VendorShowHandler {
    constructor(repository) {
        this.repository = repository;
    }
    async execute({ id }) {
        try {
            const vendor = await this.repository.findOneBy({ id });
            return (0, class_transformer_1.plainToClass)(data_structures_2.VendorDto, vendor, { excludeExtraneousValues: true });
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
};
VendorShowHandler = tslib_1.__decorate([
    (0, cqrs_1.QueryHandler)(vendor_show_query_1.VendorShowQuery),
    tslib_1.__metadata("design:paramtypes", [vendor_repository_1.VendorsRepository])
], VendorShowHandler);
exports.VendorShowHandler = VendorShowHandler;
