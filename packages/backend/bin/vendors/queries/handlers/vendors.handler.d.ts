import { IQueryHandler } from '@nestjs/cqrs';
import { PaginateVendorDto } from '@agendamanager/data-structures';
import { VendorsRepository } from '../../vendor.repository';
import { VendorPaginateQuery } from '../vendor-paginate.query';
import { VendorShowQuery } from '../vendor-show.query';
import { VendorDto } from '@agendamanager/data-structures';
export declare class VendorPaginateHandler
  implements IQueryHandler<VendorPaginateQuery>
{
  private repository;
  constructor(repository: VendorsRepository);
  execute({ vendorPaginate }: VendorPaginateQuery): Promise<PaginateVendorDto>;
}
export declare class VendorShowHandler
  implements IQueryHandler<VendorShowQuery>
{
  private repository;
  constructor(repository: VendorsRepository);
  execute({ id }: VendorShowQuery): Promise<VendorDto>;
}
