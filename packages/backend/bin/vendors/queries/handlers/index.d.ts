import { VendorPaginateHandler, VendorShowHandler } from './vendors.handler';
export declare const QueryHandlers: (
  | typeof VendorPaginateHandler
  | typeof VendorShowHandler
)[];
