import {
  ValidationOptions,
  ValidatorConstraintInterface,
} from 'class-validator';
import { VendorsRepository } from '../vendor.repository';
export declare class VendorExistsValidation
  implements ValidatorConstraintInterface
{
  private repository;
  constructor(repository: VendorsRepository);
  validate(id: string): Promise<boolean>;
  defaultMessage(): string;
}
/**
 *
 * @param {object} validationOptions not much in this case
 * @returns {function} a factory for a custom validator decorator
 */
export declare function VendorExists(
  validationOptions?: ValidationOptions
): (object: any, propertyName: string) => void;
