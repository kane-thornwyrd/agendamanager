"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorExists = exports.VendorExistsValidation = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const vendor_repository_1 = require("../vendor.repository");
let VendorExistsValidation = class VendorExistsValidation {
    constructor(repository) {
        this.repository = repository;
    }
    async validate(id) {
        try {
            const vendor = await this.repository.findOneBy({ id: +id });
            return !!vendor;
        }
        catch (err) {
            throw new common_1.InternalServerErrorException(err.message);
        }
    }
    defaultMessage() {
        return `Vendor does not exists.`;
    }
};
VendorExistsValidation = tslib_1.__decorate([
    (0, class_validator_1.ValidatorConstraint)({ name: 'vendorExists', async: true }),
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [vendor_repository_1.VendorsRepository])
], VendorExistsValidation);
exports.VendorExistsValidation = VendorExistsValidation;
/**
 *
 * @param {object} validationOptions not much in this case
 * @returns {function} a factory for a custom validator decorator
 */
function VendorExists(validationOptions) {
    return function (object, propertyName) {
        (0, class_validator_1.registerDecorator)({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            validator: VendorExistsValidation,
        });
    };
}
exports.VendorExists = VendorExists;
