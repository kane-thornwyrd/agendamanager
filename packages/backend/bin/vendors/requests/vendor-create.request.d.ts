import { BaseRequest } from './base.request';
export declare class VendorCreateRequest extends BaseRequest {
  name: string;
}
