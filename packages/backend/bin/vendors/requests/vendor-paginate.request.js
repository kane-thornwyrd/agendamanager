"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorPaginateRequest = void 0;
const paginate_request_1 = require("../../infrastructure/db/typeorm/pagination/paginate.request");
class VendorPaginateRequest extends paginate_request_1.PaginateRequest {
}
exports.VendorPaginateRequest = VendorPaginateRequest;
