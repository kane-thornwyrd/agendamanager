"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorUpdateRequest = void 0;
const swagger_1 = require("@nestjs/swagger");
const vendor_create_request_1 = require("./vendor-create.request");
class VendorUpdateRequest extends (0, swagger_1.PartialType)(vendor_create_request_1.VendorCreateRequest) {
}
exports.VendorUpdateRequest = VendorUpdateRequest;
