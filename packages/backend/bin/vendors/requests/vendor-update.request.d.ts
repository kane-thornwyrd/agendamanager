import { VendorCreateRequest } from './vendor-create.request';
declare const VendorUpdateRequest_base: import('@nestjs/common').Type<
  Partial<VendorCreateRequest>
>;
export declare class VendorUpdateRequest extends VendorUpdateRequest_base {}
export {};
