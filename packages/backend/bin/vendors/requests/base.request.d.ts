export declare class BaseRequest {
  context?: {
    params: {
      id?: string;
    };
    query: any;
    user: any;
  };
}
