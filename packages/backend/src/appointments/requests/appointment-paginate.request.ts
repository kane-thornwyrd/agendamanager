import { PaginateRequest } from '../../infrastructure/db/typeorm/pagination/paginate.request';

export class AppointmentPaginateRequest extends PaginateRequest {}
