import { Allow } from 'class-validator';

export class BaseRequest {
  @Allow()
  context?: {
    params: {
      id?: number;
    };
    query: any;
    user: any;
  };
}
