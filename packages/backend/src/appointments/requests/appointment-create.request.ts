import { IsString, MaxLength, MinLength, IsOptional } from 'class-validator';
import { BaseRequest } from './base.request';
import { ApiProperty } from '@nestjs/swagger';
import { AppointmentTypeEnum } from '@agendamanager/data-structures';

export class AppointmentCreateRequest extends BaseRequest {
  @ApiProperty()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  title: string;

  @ApiProperty()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  type: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  location: string;

  @ApiProperty({
    format: 'uri',
  })
  @IsString()
  @IsOptional()
  link: string;

  @ApiProperty()
  @IsString()
  @MinLength(3)
  host: string;

  @ApiProperty()
  @IsString()
  @MinLength(3)
  client: string;

  @ApiProperty({
    format: 'date-time',
  })
  @IsString()
  @MinLength(3)
  startTime: string;

  @ApiProperty({
    format: 'date-time',
  })
  @IsString()
  @MinLength(3)
  endTime: string;
}
