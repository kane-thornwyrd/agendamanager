import { PartialType } from '@nestjs/swagger';
import { AppointmentCreateRequest } from './appointment-create.request';

export class AppointmentUpdateRequest extends PartialType(
  AppointmentCreateRequest
) {}
