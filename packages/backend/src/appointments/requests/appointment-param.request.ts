import { IsNumber } from 'class-validator';
import { AppointmentExists } from '../validations';
import { ApiProperty } from '@nestjs/swagger';

export class AppointmentParamRequest {
  @ApiProperty()
  @IsNumber()
  @AppointmentExists()
  id: number;
}
