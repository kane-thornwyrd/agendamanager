import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import {
  PaginateAppointmentDto,
  AppointmentDto,
} from '@agendamanager/data-structures';

import {
  AppointmentCreateCommand,
  AppointmentUpdateCommand,
  AppointmentDestroyCommand,
} from './commands';

import { AppointmentPaginateQuery, AppointmentShowQuery } from './queries';

import {
  AppointmentCreateRequest,
  AppointmentPaginateRequest,
  AppointmentParamRequest,
  AppointmentUpdateRequest,
} from './requests';

@ApiTags('appointments')
@Controller('/appointment')
export class AppointmentsController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus
  ) {}

  @Get('/list')
  @ApiOperation({ summary: 'appointments Paginated' })
  @ApiResponse({
    status: 200,
    type: PaginateAppointmentDto,
  })
  async index(
    @Query() AppointmentPaginate: AppointmentPaginateRequest
  ): Promise<PaginateAppointmentDto> {
    return this.queryBus.execute(
      new AppointmentPaginateQuery(AppointmentPaginate)
    );
  }

  @Post()
  @ApiOperation({ summary: 'Create a new Appointment' })
  @ApiResponse({
    status: 201,
    type: AppointmentDto,
  })
  @HttpCode(201)
  async store(
    @Body() AppointmentCreate: AppointmentCreateRequest
  ): Promise<AppointmentDto> {
    return this.commandBus.execute(
      new AppointmentCreateCommand(AppointmentCreate)
    );
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Show a Appointment' })
  @ApiResponse({
    status: 200,
    type: AppointmentDto,
  })
  @HttpCode(200)
  async show(
    @Param() { id }: AppointmentParamRequest
  ): Promise<AppointmentDto> {
    return this.queryBus.execute(new AppointmentShowQuery(+id));
  }

  @Put('/:id')
  @ApiOperation({ summary: 'Update a Appointment' })
  @ApiResponse({
    status: 200,
    type: AppointmentDto,
  })
  @HttpCode(200)
  async update(
    @Param() { id }: AppointmentParamRequest,
    @Body() AppointmentUpdate: AppointmentUpdateRequest
  ): Promise<AppointmentDto> {
    return this.commandBus.execute(
      new AppointmentUpdateCommand(id, AppointmentUpdate)
    );
  }

  @Delete('/:id')
  @ApiOperation({ summary: 'Delete a Appointment' })
  @ApiResponse({ status: 204 })
  @HttpCode(204)
  async destroy(@Param() { id }: AppointmentParamRequest): Promise<void> {
    await this.commandBus.execute(new AppointmentDestroyCommand(id));
  }
}
