import { InternalServerErrorException } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { plainToClass } from 'class-transformer';
import {
  PaginateAppointmentDto,
  AppointmentDto,
} from '@agendamanager/data-structures';
import { AppointmentsRepository } from '../../appointments.repository';
import { AppointmentPaginateQuery } from '../appointment-paginate.query';
import { AppointmentShowQuery } from '../appointment-show.query';

@QueryHandler(AppointmentPaginateQuery)
export class AppointmentPaginateHandler
  implements IQueryHandler<AppointmentPaginateQuery>
{
  constructor(private repository: AppointmentsRepository) {}

  async execute({
    appointmentPaginate,
  }: AppointmentPaginateQuery): Promise<PaginateAppointmentDto> {
    const result = await this.repository.paginate(appointmentPaginate);
    return plainToClass(PaginateAppointmentDto, result, {
      excludeExtraneousValues: true,
    });
  }
}

@QueryHandler(AppointmentShowQuery)
export class AppointmentShowHandler
  implements IQueryHandler<AppointmentShowQuery>
{
  constructor(private repository: AppointmentsRepository) {}

  async execute({ id }: AppointmentShowQuery): Promise<AppointmentDto> {
    try {
      const appointment = await this.repository.findOneBy({ id });
      return plainToClass(AppointmentDto, appointment, {
        excludeExtraneousValues: true,
      });
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
