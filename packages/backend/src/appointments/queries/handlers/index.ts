import {
  AppointmentPaginateHandler,
  AppointmentShowHandler,
} from './appointments.handler';

export const QueryHandlers = [
  AppointmentPaginateHandler,
  AppointmentShowHandler,
];
