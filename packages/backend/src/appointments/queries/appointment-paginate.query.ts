import { AppointmentPaginateRequest } from '../requests';

export class AppointmentPaginateQuery {
  constructor(
    public readonly appointmentPaginate: AppointmentPaginateRequest
  ) {}
}
