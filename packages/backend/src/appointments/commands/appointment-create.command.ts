import { AppointmentCreateRequest } from '../requests';

export class AppointmentCreateCommand {
  constructor(public readonly AppointmentCreate: AppointmentCreateRequest) {}
}
