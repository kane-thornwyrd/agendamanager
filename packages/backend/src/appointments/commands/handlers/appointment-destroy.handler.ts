import { InternalServerErrorException } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AppointmentsRepository } from '../../appointments.repository';
import { AppointmentDestroyCommand } from '../appointment-destroy.command';

@CommandHandler(AppointmentDestroyCommand)
export class AppointmentDestroyHandler
  implements ICommandHandler<AppointmentDestroyCommand>
{
  constructor(private repository: AppointmentsRepository) {}

  async execute({ id }: AppointmentDestroyCommand): Promise<void> {
    try {
      const appointment = await this.repository.findOneBy({ id });
      await this.repository.remove(appointment);
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
