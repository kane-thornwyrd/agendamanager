import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AppointmentDto } from '@agendamanager/data-structures';
import { plainToClass } from 'class-transformer';
import { InternalServerErrorException } from '@nestjs/common';

import { AppointmentsRepository } from '../../appointments.repository';

import { AppointmentCreateCommand } from '../appointment-create.command';

// TODO REFACTOR USING FACTORY AND EventPublisher !!!!
@CommandHandler(AppointmentCreateCommand)
export class AppointmentCreateHandler
  implements ICommandHandler<AppointmentCreateCommand>
{
  constructor(private repository: AppointmentsRepository) {}

  async execute({
    AppointmentCreate: appointment,
  }: AppointmentCreateCommand): Promise<AppointmentDto> {
    try {
      const newAppointment = this.repository.create();
      Object.keys(appointment).forEach((key) => {
        const value: any = appointment[key];
        newAppointment[key] = value;
      });

      await this.repository.save(newAppointment);
      return plainToClass(AppointmentDto, newAppointment, {
        excludeExtraneousValues: true,
      });
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
