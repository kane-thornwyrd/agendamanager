import { InternalServerErrorException } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { plainToClass } from 'class-transformer';
import { AppointmentDto } from '@agendamanager/data-structures';
import { AppointmentsRepository } from '../../appointments.repository';
import { AppointmentUpdateCommand } from '../appointment-update.command';

@CommandHandler(AppointmentUpdateCommand)
export class AppointmentUpdateHandler
  implements ICommandHandler<AppointmentUpdateCommand>
{
  constructor(private repository: AppointmentsRepository) {}

  async execute({
    id,
    appointmentUpdate,
  }: AppointmentUpdateCommand): Promise<AppointmentDto> {
    try {
      const appointment = await this.repository.findOneBy({ id });
      Object.keys(appointmentUpdate).forEach((key) => {
        const value: any = appointmentUpdate[key];
        appointment[key] = value;
      });
      await this.repository.save(appointment);
      return plainToClass(AppointmentDto, appointment, {
        excludeExtraneousValues: true,
      });
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
