import { AppointmentUpdateRequest } from '../requests';

export class AppointmentUpdateCommand {
  constructor(
    public readonly id: number,
    public readonly appointmentUpdate: AppointmentUpdateRequest
  ) {}
}
