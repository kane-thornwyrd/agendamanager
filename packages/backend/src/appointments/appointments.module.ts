import { AppointmentEntity } from '@agendamanager/data-structures';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmSplitterModule } from '../infrastructure/db/typeorm/typeorm-splitter.module';
import { CommandHandlers } from './commands/handlers';
import { QueryHandlers } from './queries/handlers';
import { AppointmentsRepository } from './appointments.repository';
import { AppointmentExistsValidation } from './validations';
import { AppointmentsController } from './appointments.controller';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([AppointmentEntity]),
    TypeOrmSplitterModule.forCustomRepository([AppointmentsRepository]),
  ],
  controllers: [AppointmentsController],
  providers: [
    AppointmentExistsValidation,
    ...CommandHandlers,
    ...QueryHandlers,
  ],
})
export class AppointmentsModule {}
