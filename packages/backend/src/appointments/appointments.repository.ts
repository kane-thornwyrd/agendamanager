import { Injectable } from '@nestjs/common';
import { AppointmentEntity } from '@agendamanager/data-structures';

import { BaseRepository } from '../infrastructure/db/typeorm/base.repository';
import { CustomRepository } from '../infrastructure/db/typeorm/typeorm-splitter.decorator';

@Injectable()
@CustomRepository(AppointmentEntity)
export class AppointmentsRepository extends BaseRepository<AppointmentEntity> {}
