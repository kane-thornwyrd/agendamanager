import { Injectable, InternalServerErrorException } from '@nestjs/common';
import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { AppointmentsRepository } from '../appointments.repository';

@ValidatorConstraint({ name: 'appointmentExists', async: true })
@Injectable()
export class AppointmentExistsValidation
  implements ValidatorConstraintInterface
{
  constructor(private repository: AppointmentsRepository) {}

  async validate(id: string) {
    try {
      const appointment = await this.repository.findOneBy({ id: +id });
      return !!appointment;
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }

  defaultMessage() {
    return `Appointment does not exists.`;
  }
}

/**
 *
 * @param {object} validationOptions not much in this case
 * @returns {function} a factory for a custom validator decorator
 */
export function AppointmentExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: AppointmentExistsValidation,
    });
  };
}
