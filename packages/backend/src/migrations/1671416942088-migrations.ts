import { MigrationInterface, QueryRunner } from 'typeorm';

export class migrations1671416942088 implements MigrationInterface {
  name = 'migrations1671416942088';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "appointment" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar(255) NOT NULL, "type" varchar NOT NULL DEFAULT ('physical'), "location" text, "link" text, "host" varchar(255) NOT NULL, "client" varchar(255) NOT NULL, "startTime" datetime NOT NULL, "endTime" datetime NOT NULL)`
    );
    await queryRunner.query(
      `CREATE TABLE "buyer" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255) NOT NULL, "company" varchar(255) NOT NULL)`
    );
    await queryRunner.query(
      `CREATE TABLE "vendor" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255) NOT NULL)`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "vendor"`);
    await queryRunner.query(`DROP TABLE "buyer"`);
    await queryRunner.query(`DROP TABLE "appointment"`);
  }
}
