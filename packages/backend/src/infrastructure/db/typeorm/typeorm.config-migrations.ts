import * as dotenv from 'dotenv';
import { DataSource, DataSourceOptions } from 'typeorm';

dotenv.config();

const typeOrmConfig = new DataSource({
  type: process.env.DATABASE_CONNECTION_TYPE,
  database: process.env.DATABASE_FILE,
  entities: [`${__dirname}/../../../**/*.entity{.js,.ts}`],
  synchronize: false,
  logging: true,
  migrations: [`${__dirname}/../../../migrations/*{.js,.ts}`],
} as DataSourceOptions);

export default typeOrmConfig;
