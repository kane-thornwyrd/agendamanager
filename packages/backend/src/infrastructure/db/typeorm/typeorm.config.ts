import { ConfigService } from '@nestjs/config';
import {
  TypeOrmModuleAsyncOptions,
  TypeOrmModuleOptions,
} from '@nestjs/typeorm';

import * as PATH from 'path';

console.log(PATH.resolve(`${__dirname}/../../../**/*.entity{.js,.ts}`));

export const typeOrmAsyncConfig: TypeOrmModuleAsyncOptions = {
  useFactory: (configService: ConfigService) =>
    configService.get<string>('NODE_ENV') !== 'test'
      ? ({
          type: configService.get<string>('DATABASE_CONNECTION_TYPE'),
          database: configService.get<string>('DATABASE_FILE'),
          entities: [`${__dirname}/../../../**/*.entity{.js,.ts}`],
          synchronize: false,
          logging:
            configService.get<string>('NODE_ENV') !== 'production'
              ? true
              : false,
        } as TypeOrmModuleOptions)
      : ({
          type: 'sqlite',
          database: ':memory:',
          entities: [`${__dirname}/../../../**/*.entity{.js,.ts}`],
          synchronize: true,
          logging: false,
          dropSchema: true,
        } as TypeOrmModuleOptions),
  inject: [ConfigService],
};
