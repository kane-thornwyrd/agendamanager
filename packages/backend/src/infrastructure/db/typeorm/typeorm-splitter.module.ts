import { DynamicModule, Provider } from '@nestjs/common';
import { getDataSourceToken } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { TYPEORM_SPLITTER_CUSTOM_REPOSITORY } from './typeorm-splitter.decorator';

/**
 * Module that will use the metadata `TYPEORM_SPLITTER_CUSTOM_REPOSITORY` to
 * get the correct config to use a specific repo for a specific type of entity.
 * @returns {DynamicModule} Module to import (put in the `imports` array) for a
 * CQRS-style module to inject the custom repository for the target module entity.
 *
 * @example
 * [at]Module({
 *   imports: [
 *     CqrsModule,
 *     TypeOrmModule.forFeature([MyEntity]),
 *     TypeOrmSplitterModule.forCustomRepository([MyRepository]),
 *   ],
 *   controllers: [MyController],
 *   providers: [
 *     ...MyValidation1,
 *     ...MyValidation2,
 *     ...MyValidationX,
 *     ...CommandHandlers,
 *     ...QueryHandlers,
 *   ],
 * })
 * export class MyModule {}
 *
 **/
export class TypeOrmSplitterModule {
  public static forCustomRepository<T extends new (...args: any[]) => any>(
    repositories: T[]
  ): DynamicModule {
    const providers: Provider[] = [];

    for (const repository of repositories) {
      const entity = Reflect.getMetadata(
        TYPEORM_SPLITTER_CUSTOM_REPOSITORY,
        repository
      );

      if (!entity) {
        continue;
      }

      providers.push({
        inject: [getDataSourceToken()],
        provide: repository,
        useFactory: (dataSource: DataSource): typeof repository => {
          const baseRepository = dataSource.getRepository<any>(entity);
          return new repository(
            baseRepository.target,
            baseRepository.manager,
            baseRepository.queryRunner
          );
        },
      });
    }

    return {
      exports: providers,
      module: TypeOrmSplitterModule,
      providers,
    };
  }
}
