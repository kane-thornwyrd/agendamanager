import { DocumentBuilder } from '@nestjs/swagger';

export const swaggerConfig = new DocumentBuilder()
  .setTitle('AgendaManager')
  .setDescription('AgendaManager Api documentation')
  .setVersion('1.0')
  .addTag('vendors')
  .addTag('buyers')
  .addTag('appointments')
  .build();
