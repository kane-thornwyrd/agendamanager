import { BuyerPaginateRequest } from '../requests';

export class BuyerPaginateQuery {
  constructor(public readonly buyerPaginate: BuyerPaginateRequest) {}
}
