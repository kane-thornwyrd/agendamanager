import { BuyerPaginateHandler, BuyerShowHandler } from './buyers.handler';

export const QueryHandlers = [BuyerPaginateHandler, BuyerShowHandler];
