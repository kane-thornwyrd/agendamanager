import { InternalServerErrorException } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { plainToClass } from 'class-transformer';
import { PaginateBuyerDto } from '@agendamanager/data-structures';
import { BuyersRepository } from '../../buyers.repository';
import { BuyerPaginateQuery } from '../buyer-paginate.query';
import { BuyerShowQuery } from '../buyer-show.query';
import { BuyerDto } from '@agendamanager/data-structures';

@QueryHandler(BuyerPaginateQuery)
export class BuyerPaginateHandler implements IQueryHandler<BuyerPaginateQuery> {
  constructor(private repository: BuyersRepository) {}

  async execute({
    buyerPaginate,
  }: BuyerPaginateQuery): Promise<PaginateBuyerDto> {
    const result = await this.repository.paginate(buyerPaginate);
    return plainToClass(PaginateBuyerDto, result, {
      excludeExtraneousValues: true,
    });
  }
}

@QueryHandler(BuyerShowQuery)
export class BuyerShowHandler implements IQueryHandler<BuyerShowQuery> {
  constructor(private repository: BuyersRepository) {}

  async execute({ id }: BuyerShowQuery): Promise<BuyerDto> {
    try {
      const buyer = await this.repository.findOneBy({ id });
      return plainToClass(BuyerDto, buyer, { excludeExtraneousValues: true });
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
