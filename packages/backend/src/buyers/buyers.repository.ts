import { Injectable } from '@nestjs/common';
import { BuyerEntity } from '@agendamanager/data-structures';

import { BaseRepository } from '../infrastructure/db/typeorm/base.repository';
import { CustomRepository } from '../infrastructure/db/typeorm/typeorm-splitter.decorator';

@Injectable()
@CustomRepository(BuyerEntity)
export class BuyersRepository extends BaseRepository<BuyerEntity> {}
