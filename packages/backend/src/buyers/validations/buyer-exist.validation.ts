import { Injectable, InternalServerErrorException } from '@nestjs/common';
import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { BuyersRepository } from '../buyers.repository';

@ValidatorConstraint({ name: 'buyerExists', async: true })
@Injectable()
export class BuyerExistsValidation implements ValidatorConstraintInterface {
  constructor(private repository: BuyersRepository) {}

  async validate(id: string) {
    try {
      const buyer = await this.repository.findOneBy({ id: +id });
      return !!buyer;
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }

  defaultMessage() {
    return `Buyer does not exists.`;
  }
}

/**
 *
 * @param {object} validationOptions not much in this case
 * @returns {function} a factory for a custom validator decorator
 */
export function BuyerExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: BuyerExistsValidation,
    });
  };
}
