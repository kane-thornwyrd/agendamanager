import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { PaginateBuyerDto, BuyerDto } from '@agendamanager/data-structures';

import {
  BuyerCreateCommand,
  BuyerUpdateCommand,
  BuyerDestroyCommand,
} from './commands';

import { BuyerPaginateQuery, BuyerShowQuery } from './queries';

import {
  BuyerCreateRequest,
  BuyerPaginateRequest,
  BuyerParamRequest,
  BuyerUpdateRequest,
} from './requests';

@ApiTags('buyers')
@Controller('/buyer')
export class BuyersController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus
  ) {}

  @Get('/list')
  @ApiOperation({ summary: 'buyers Paginated' })
  @ApiResponse({
    status: 200,
    type: PaginateBuyerDto,
  })
  async index(
    @Query() BuyerPaginate: BuyerPaginateRequest
  ): Promise<PaginateBuyerDto> {
    return this.queryBus.execute(new BuyerPaginateQuery(BuyerPaginate));
  }

  @Post()
  @ApiOperation({ summary: 'Create a new Buyer' })
  @ApiResponse({
    status: 201,
    type: BuyerDto,
  })
  @HttpCode(201)
  async store(@Body() BuyerCreate: BuyerCreateRequest): Promise<BuyerDto> {
    return this.commandBus.execute(new BuyerCreateCommand(BuyerCreate));
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Show a Buyer' })
  @ApiResponse({
    status: 200,
    type: BuyerDto,
  })
  @HttpCode(200)
  async show(@Param() { id }: BuyerParamRequest): Promise<BuyerDto> {
    return this.queryBus.execute(new BuyerShowQuery(+id));
  }

  @Put('/:id')
  @ApiOperation({ summary: 'Update a Buyer' })
  @ApiResponse({
    status: 200,
    type: BuyerDto,
  })
  @HttpCode(200)
  async update(
    @Param() { id }: BuyerParamRequest,
    @Body() BuyerUpdate: BuyerUpdateRequest
  ): Promise<BuyerDto> {
    return this.commandBus.execute(new BuyerUpdateCommand(id, BuyerUpdate));
  }

  @Delete('/:id')
  @ApiOperation({ summary: 'Delete a Buyer' })
  @ApiResponse({ status: 204 })
  @HttpCode(204)
  async destroy(@Param() { id }: BuyerParamRequest): Promise<void> {
    await this.commandBus.execute(new BuyerDestroyCommand(id));
  }
}
