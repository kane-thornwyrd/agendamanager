import { IsNumber } from 'class-validator';
import { BuyerExists } from '../validations';
import { ApiProperty } from '@nestjs/swagger';

export class BuyerParamRequest {
  @ApiProperty()
  @IsNumber()
  @BuyerExists()
  id: number;
}
