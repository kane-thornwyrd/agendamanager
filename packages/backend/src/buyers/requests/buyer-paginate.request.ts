import { PaginateRequest } from '../../infrastructure/db/typeorm/pagination/paginate.request';

export class BuyerPaginateRequest extends PaginateRequest {}
