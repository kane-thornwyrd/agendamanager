import { PartialType } from '@nestjs/swagger';
import { BuyerCreateRequest } from './buyer-create.request';

export class BuyerUpdateRequest extends PartialType(BuyerCreateRequest) {}
