import { IsString, MaxLength, MinLength } from 'class-validator';
import { BaseRequest } from './base.request';
import { ApiProperty } from '@nestjs/swagger';

export class BuyerCreateRequest extends BaseRequest {
  @ApiProperty()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  name: string;

  @ApiProperty()
  @IsString()
  @MinLength(3)
  @MaxLength(255)
  company: string;
}
