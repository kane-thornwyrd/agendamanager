import { BuyerCreateRequest } from '../requests';

export class BuyerCreateCommand {
  constructor(public readonly BuyerCreate: BuyerCreateRequest) {}
}
