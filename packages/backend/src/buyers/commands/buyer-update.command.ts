import { BuyerUpdateRequest } from '../requests';

export class BuyerUpdateCommand {
  constructor(
    public readonly id: number,
    public readonly buyerUpdate: BuyerUpdateRequest
  ) {}
}
