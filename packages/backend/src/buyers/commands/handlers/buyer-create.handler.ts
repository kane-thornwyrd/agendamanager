import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BuyerDto } from '@agendamanager/data-structures';
import { plainToClass } from 'class-transformer';
import { InternalServerErrorException } from '@nestjs/common';

import { BuyersRepository } from '../../buyers.repository';

import { BuyerCreateCommand } from '../buyer-create.command';

@CommandHandler(BuyerCreateCommand)
export class BuyerCreateHandler implements ICommandHandler<BuyerCreateCommand> {
  constructor(private repository: BuyersRepository) {}

  async execute({
    BuyerCreate: { name, company },
  }: BuyerCreateCommand): Promise<BuyerDto> {
    try {
      const newBuyer = this.repository.create();
      newBuyer.name = name;
      newBuyer.company = company;

      await this.repository.save(newBuyer);
      return plainToClass(BuyerDto, newBuyer, {
        excludeExtraneousValues: true,
      });
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
