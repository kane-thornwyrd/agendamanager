import { InternalServerErrorException } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BuyersRepository } from '../../buyers.repository';
import { BuyerDestroyCommand } from '../buyer-destroy.command';

@CommandHandler(BuyerDestroyCommand)
export class BuyerDestroyHandler
  implements ICommandHandler<BuyerDestroyCommand>
{
  constructor(private repository: BuyersRepository) {}

  async execute({ id }: BuyerDestroyCommand): Promise<void> {
    try {
      const buyer = await this.repository.findOneBy({ id });
      await this.repository.remove(buyer);
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
