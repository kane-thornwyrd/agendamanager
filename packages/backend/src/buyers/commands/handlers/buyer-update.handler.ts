import { InternalServerErrorException } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { plainToClass } from 'class-transformer';
import { BuyerDto } from '@agendamanager/data-structures';
import { BuyersRepository } from '../../buyers.repository';
import { BuyerUpdateCommand } from '../buyer-update.command';

@CommandHandler(BuyerUpdateCommand)
export class BuyerUpdateHandler implements ICommandHandler<BuyerUpdateCommand> {
  constructor(private repository: BuyersRepository) {}

  async execute({ id, buyerUpdate }: BuyerUpdateCommand): Promise<BuyerDto> {
    try {
      const buyer = await this.repository.findOneBy({ id });
      Object.keys(buyerUpdate).forEach((key) => {
        const value: any = buyerUpdate[key];
        buyer[key] = value;
      });
      await this.repository.save(buyer);
      return plainToClass(BuyerDto, buyer, { excludeExtraneousValues: true });
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
