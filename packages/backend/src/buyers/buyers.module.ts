import { BuyerEntity } from '@agendamanager/data-structures';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmSplitterModule } from '../infrastructure/db/typeorm/typeorm-splitter.module';
import { CommandHandlers } from './commands/handlers';
import { QueryHandlers } from './queries/handlers';
import { BuyersRepository } from './buyers.repository';
import { BuyerExistsValidation } from './validations';
import { BuyersController } from './buyers.controller';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([BuyerEntity]),
    TypeOrmSplitterModule.forCustomRepository([BuyersRepository]),
  ],
  controllers: [BuyersController],
  providers: [BuyerExistsValidation, ...CommandHandlers, ...QueryHandlers],
})
export class BuyersModule {}
