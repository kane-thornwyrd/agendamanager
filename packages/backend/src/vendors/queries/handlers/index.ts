import { VendorPaginateHandler, VendorShowHandler } from './vendors.handler';

export const QueryHandlers = [VendorPaginateHandler, VendorShowHandler];
