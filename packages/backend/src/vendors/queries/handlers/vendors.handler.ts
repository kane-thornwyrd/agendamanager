import { InternalServerErrorException } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { plainToClass } from 'class-transformer';
import { PaginateVendorDto } from '@agendamanager/data-structures';
import { VendorsRepository } from '../../vendor.repository';
import { VendorPaginateQuery } from '../vendor-paginate.query';
import { VendorShowQuery } from '../vendor-show.query';
import { VendorDto } from '@agendamanager/data-structures';

@QueryHandler(VendorPaginateQuery)
export class VendorPaginateHandler
  implements IQueryHandler<VendorPaginateQuery>
{
  constructor(private repository: VendorsRepository) {}

  async execute({
    vendorPaginate,
  }: VendorPaginateQuery): Promise<PaginateVendorDto> {
    const result = await this.repository.paginate(vendorPaginate);
    return plainToClass(PaginateVendorDto, result, {
      excludeExtraneousValues: true,
    });
  }
}

@QueryHandler(VendorShowQuery)
export class VendorShowHandler implements IQueryHandler<VendorShowQuery> {
  constructor(private repository: VendorsRepository) {}

  async execute({ id }: VendorShowQuery): Promise<VendorDto> {
    try {
      const vendor = await this.repository.findOneBy({ id });
      return plainToClass(VendorDto, vendor, { excludeExtraneousValues: true });
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
