import { VendorPaginateRequest } from '../requests';

export class VendorPaginateQuery {
  constructor(public readonly vendorPaginate: VendorPaginateRequest) {}
}
