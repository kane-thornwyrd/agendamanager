import { IsString, MaxLength, MinLength } from 'class-validator';
import { BaseRequest } from './base.request';
import { ApiProperty } from '@nestjs/swagger';

export class VendorCreateRequest extends BaseRequest {
  @ApiProperty()
  @IsString()
  @MinLength(3)
  @MaxLength(80)
  name: string;
}
