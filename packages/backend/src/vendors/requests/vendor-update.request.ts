import { PartialType } from '@nestjs/swagger';
import { VendorCreateRequest } from './vendor-create.request';

export class VendorUpdateRequest extends PartialType(VendorCreateRequest) {}
