import { IsString } from 'class-validator';
import { VendorExists } from '../validations';
import { ApiProperty } from '@nestjs/swagger';

export class VendorParamRequest {
  @ApiProperty()
  @IsString()
  @VendorExists()
  id: string;
}
