import { Injectable } from '@nestjs/common';
import { VendorEntity } from '@agendamanager/data-structures';

import { BaseRepository } from '../infrastructure/db/typeorm/base.repository';
import { CustomRepository } from '../infrastructure/db/typeorm/typeorm-splitter.decorator';

@Injectable()
@CustomRepository(VendorEntity)
export class VendorsRepository extends BaseRepository<VendorEntity> {}
