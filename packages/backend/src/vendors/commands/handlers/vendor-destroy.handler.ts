import { InternalServerErrorException } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { VendorsRepository } from '../../vendor.repository';
import { VendorDestroyCommand } from '../vendor-destroy.command';

@CommandHandler(VendorDestroyCommand)
export class VendorDestroyHandler
  implements ICommandHandler<VendorDestroyCommand>
{
  constructor(private repository: VendorsRepository) {}

  async execute({ id }: VendorDestroyCommand): Promise<void> {
    try {
      const vendor = await this.repository.findOneBy({ id: +id });
      await this.repository.remove(vendor);
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
