import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { VendorDto } from '@agendamanager/data-structures';
import { VendorsRepository } from '../../vendor.repository';
import { ConsoleLogger, InternalServerErrorException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';

import { VendorCreateCommand } from '../vendor-create.command';

@CommandHandler(VendorCreateCommand)
export class VendorCreateHandler
  implements ICommandHandler<VendorCreateCommand>
{
  constructor(private repository: VendorsRepository) {}

  async execute({
    vendorCreate: { name },
  }: VendorCreateCommand): Promise<VendorDto> {
    try {
      const newVendor = this.repository.create();
      newVendor.name = name;

      await this.repository.save(newVendor);
      return plainToClass(VendorDto, newVendor, {
        excludeExtraneousValues: true,
      });
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
