import { InternalServerErrorException } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { plainToClass } from 'class-transformer';
import { VendorDto } from '@agendamanager/data-structures';

import { VendorsRepository } from '../../vendor.repository';
import { VendorUpdateCommand } from '../vendor-update.command';

@CommandHandler(VendorUpdateCommand)
export class VendorUpdateHandler
  implements ICommandHandler<VendorUpdateCommand>
{
  constructor(private repository: VendorsRepository) {}

  async execute({ id, vendorUpdate }: VendorUpdateCommand): Promise<VendorDto> {
    try {
      const vendor = await this.repository.findOneBy({ id: +id });
      Object.keys(vendorUpdate).forEach((key) => {
        const value: any = vendorUpdate[key];
        vendor[key] = value;
      });
      await this.repository.save(vendor);
      return plainToClass(VendorDto, vendor, { excludeExtraneousValues: true });
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
