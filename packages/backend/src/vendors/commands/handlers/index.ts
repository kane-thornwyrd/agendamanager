import { VendorCreateHandler } from './vendor-create.handler';
import { VendorUpdateHandler } from './vendor-update.handler';
import { VendorDestroyHandler } from './vendor-destroy.handler';

export const CommandHandlers = [
  VendorCreateHandler,
  VendorUpdateHandler,
  VendorDestroyHandler,
];
