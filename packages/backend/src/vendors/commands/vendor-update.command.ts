import { VendorUpdateRequest } from '../requests';

export class VendorUpdateCommand {
  constructor(
    public readonly id: string,
    public readonly vendorUpdate: VendorUpdateRequest
  ) {}
}
