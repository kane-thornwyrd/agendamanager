import { VendorCreateRequest } from '../requests';

export class VendorCreateCommand {
  constructor(public readonly vendorCreate: VendorCreateRequest) {}
}
