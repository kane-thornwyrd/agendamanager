import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { PaginateVendorDto, VendorDto } from '@agendamanager/data-structures';

import {
  VendorCreateCommand,
  VendorUpdateCommand,
  VendorDestroyCommand,
} from './commands';

import { VendorPaginateQuery, VendorShowQuery } from './queries';

import {
  VendorCreateRequest,
  VendorPaginateRequest,
  VendorParamRequest,
  VendorUpdateRequest,
} from './requests';

@ApiTags('vendors')
@Controller('/vendor')
export class VendorsController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus
  ) {}

  @Get('/list')
  @ApiOperation({ summary: 'Vendors Paginated' })
  @ApiResponse({
    status: 200,
    type: PaginateVendorDto,
  })
  async index(
    @Query() vendorPaginate: VendorPaginateRequest
  ): Promise<PaginateVendorDto> {
    return this.queryBus.execute(new VendorPaginateQuery(vendorPaginate));
  }

  @Post()
  @ApiOperation({ summary: 'Create a new Vendor' })
  @ApiResponse({
    status: 201,
    type: VendorDto,
  })
  @HttpCode(201)
  async store(@Body() vendorCreate: VendorCreateRequest): Promise<VendorDto> {
    return this.commandBus.execute(new VendorCreateCommand(vendorCreate));
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Show a Vendor' })
  @ApiResponse({
    status: 200,
    type: VendorDto,
  })
  @HttpCode(200)
  async show(@Param() { id }: VendorParamRequest): Promise<VendorDto> {
    return this.queryBus.execute(new VendorShowQuery(parseInt(id, 10)));
  }

  @Put('/:id')
  @ApiOperation({ summary: 'Update a Vendor' })
  @ApiResponse({
    status: 200,
    type: VendorDto,
  })
  @HttpCode(200)
  async update(
    @Param() { id }: VendorParamRequest,
    @Body() vendorUpdate: VendorUpdateRequest
  ): Promise<VendorDto> {
    return this.commandBus.execute(new VendorUpdateCommand(id, vendorUpdate));
  }

  @Delete('/:id')
  @ApiOperation({ summary: 'Delete a Vendor' })
  @ApiResponse({ status: 204 })
  @HttpCode(204)
  async destroy(@Param() { id }: VendorParamRequest): Promise<void> {
    await this.commandBus.execute(new VendorDestroyCommand(id));
  }
}
