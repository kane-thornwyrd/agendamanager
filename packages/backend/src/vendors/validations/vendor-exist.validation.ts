import { Injectable, InternalServerErrorException } from '@nestjs/common';
import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { VendorsRepository } from '../vendor.repository';

@ValidatorConstraint({ name: 'vendorExists', async: true })
@Injectable()
export class VendorExistsValidation implements ValidatorConstraintInterface {
  constructor(private repository: VendorsRepository) {}

  async validate(id: string) {
    try {
      const vendor = await this.repository.findOneBy({ id: +id });
      return !!vendor;
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }

  defaultMessage() {
    return `Vendor does not exists.`;
  }
}

/**
 *
 * @param {object} validationOptions not much in this case
 * @returns {function} a factory for a custom validator decorator
 */
export function VendorExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: VendorExistsValidation,
    });
  };
}
