import { VendorEntity } from '@agendamanager/data-structures';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmSplitterModule } from '../infrastructure/db/typeorm/typeorm-splitter.module';
import { CommandHandlers } from './commands/handlers';
import { QueryHandlers } from './queries/handlers';
import { VendorsRepository } from './vendor.repository';
import { VendorExistsValidation } from './validations';
import { VendorsController } from './vendors.controller';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([VendorEntity]),
    TypeOrmSplitterModule.forCustomRepository([VendorsRepository]),
  ],
  controllers: [VendorsController],
  providers: [VendorExistsValidation, ...CommandHandlers, ...QueryHandlers],
})
export class VendorsModule {}
