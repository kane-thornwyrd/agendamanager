import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { DatabaseModule } from './infrastructure/db/db.module';
import { AppointmentsModule } from './appointments/appointments.module';
import { VendorsModule } from './vendors/vendors.module';
import { BuyersModule } from './buyers/buyers.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: Joi.object({
        PORT: Joi.number().required(),
        DATABASE_CONNECTION_TYPE: Joi.string().required(),
        DATABASE_FILE: Joi.string().required(),
      }),
    }),
    DatabaseModule,
    AppointmentsModule,
    BuyersModule,
    VendorsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
