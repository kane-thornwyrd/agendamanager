import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import { ContextInterceptor } from './infrastructure/context.interceptor';
import { swaggerConfig } from './infrastructure/swagger.conf';
import { AppModule } from './app.module';

/**
 * @returns {void}
 */
(async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
    })
  );
  app.useGlobalInterceptors(new ContextInterceptor());
  const configService = app.get(ConfigService);
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('api', app, document);
  await app.listen(configService.get<number>('PORT'));
})();
