module.exports = {
  root: true,
  env: {
    node: true,
  },
  plugins: ["@typescript-eslint/eslint-plugin"],
  extends: ["plugin:@typescript-eslint/recommended", "prettier"],
  overrides: [
    {
      files: ["**/*.ts"],
      parser: "@typescript-eslint/parser",
      parserOptions: {
        project: "tsconfig.json",
        sourceType: "module",
      },
      rules: {
        "@typescript-eslint/interface-name-prefix": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/no-unused-vars": "off",
        "@typescript-eslint/ban-types": "off",
        "arrow-body-style": "off",
        "comma-dangle": [
          "error",
          {
            arrays: "always-multiline",
            objects: "always-multiline",
            imports: "always-multiline",
            exports: "always-multiline",
            functions: "never",
          },
        ],
        "eol-last": "error",
        "linebreak-style": ["error", "unix"],
        "max-len": ["error", 120],
        "no-param-reassign": ["error", { props: false }],
        "no-plusplus": ["error", { allowForLoopAfterthoughts: true }],
        "no-restricted-syntax": [
          "error",
          {
            selector: "ForInStatement",
            message:
              "for..in loops iterate over the entire prototype chain, which is virtually never what you want. Use Object.{keys,values,entries}, and iterate over the resulting array.",
          },
          {
            selector: "LabeledStatement",
            message:
              "Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand.",
          },
          {
            selector: "WithStatement",
            message:
              "`with` is disallowed in strict mode because it makes code impossible to predict and optimize.",
          },
        ],
        "no-underscore-dangle": [
          "error",
          { allow: ["_id", "_source", "_scroll_id"] },
        ],
        "no-use-before-define": ["error", "nofunc"],
        quotes: [
          "error",
          "single",
          { avoidEscape: true, allowTemplateLiterals: true },
        ],
        "require-jsdoc": "warn",
        "require-yield": "warn",
        "valid-jsdoc": "warn",
        "arrow-parens": ["error", "always"],
      },
    },
    {
      files: ["**/*.spec.ts"],
      parser: "@typescript-eslint/parser",
      parserOptions: {
        project: "tsconfig.spec.json",
        sourceType: "module",
      },
      rules: {
        "@typescript-eslint/interface-name-prefix": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/no-unused-vars": "off",
        "@typescript-eslint/ban-types": "off",
        "@typescript-eslint/no-empty-function": "off",
      },
    },
  ],
};
