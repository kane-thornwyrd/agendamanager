# Agenda Manager
<!--
## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/kane-thornwyrd/agendamanager/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

*** -->

## Description
Just an exercise to show off.

It use npm workspaces, no need for lerna or any other exotic solution.
The `save-exact` option is enabled for `npm`, and `package-lock.json` too (by default), thus no need for `yarn` or the other one with a bunch of "p" in its name.
`nvm` is strongly suggested, but you can use any node 19.x.x you may have on hand.

highly inspired by Nest.js own repository and the `nest-cqrs-typeorm` repo from [Willian Souza](https://github.com/wrsouza), so, if you can, give them a buck or two.

## Installation

Copy to `.env` the `.env.example` in `packages/backend`.

Edit the `.env` to whatever you see fit.

```shell
$ npm i
$ npm run migration:run -w @agendamanager/backend
$ npm run start:debug -w @agendamanager/backend
```

The Swagger should be available at `http://localhost:[PORT]/api`;

## Usage
### To run a npm script from a sub-package
`$ npm run [script name] -w [subpackage name]`

__**exemple**__:

`$ npm run test:watch -w @agendamanager/backend`

### To install a module to a sub-package
`$ npm i -S[D] [module...] -w [subpackage name]`

__**exemple**__:

`$ npm i -SD @types/express @types/jest -w @agendamanager/backend`

__**note**__: we have to use `force` (in `.npmrc`) because versioning issues from `@commitlint/cli` and `commitlint-circle`.

## Support

## Authors and acknowledgment

## License
